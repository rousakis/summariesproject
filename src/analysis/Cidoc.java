/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import analysis.measures.VersionAnalysis;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class Cidoc {

    private static void InsertVersions(Properties prop, String dataset, String v) throws Exception {
        DatasetsManager mgr = new DatasetsManager(prop, dataset);
        mgr.insertDataset(dataset, "cidoc crm", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_v3.2.1.rdfs", RDFFormat.RDFXML, v + "1", "v3.2.1", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_v3.3.2.rdfs", RDFFormat.RDFXML, v + "2", "v3.3.2", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_v3.4.9.rdfs", RDFFormat.RDFXML, v + "3", "v3.4.9", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_crm_v5.1-draft-2014March.rdfs", RDFFormat.RDFXML, v + "4", "v5.1", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_crm_v6.2-draft-2015August.rdfs", RDFFormat.RDFXML, v + "5", "v6.2", dataset);
        mgr.terminate();
        //////////
    }

    public static void main(String[] args) throws Exception {
        String datasetUri = "http://CIDOC/";
        Properties prop = new Properties();
        Properties explProp = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        inputStream = new FileInputStream("explanations/explanations.properties");
        explProp.load(inputStream);
        inputStream.close();
        DatasetsManager dmgr = new DatasetsManager(prop, datasetUri);
//        dmgr.deleteDataset(true, true);
//        InsertVersions(prop, datasetUri, datasetUri);
        ////
//        Utils.correctClassesType(dmgr);
//        Utils.correctPropertiesType(dmgr);
//        String path = "input\\Cidoc\\URIs_all_versions_CIDOC\\last\\";
//        File folder = new File(path);
//        String versionPrefix = datasetUri;
//        Utils.clearChangesOntologies(prop, datasetUri);
//        Utils.DeleteAllDefinedComplexChanges(prop, datasetUri);
//        Utils.DefineComplexChanges(prop, datasetUri, "input\\CIDOC\\");
//       
//        MCDUtils utils = new MCDUtils(prop, datasetUri, false);
//        utils.detectDatasets(false);
//        utils.terminate();
//        parseFolder(folder, dmgr, versionPrefix);
        //        ChangesExploiter expl = new ChangesExploiter(dmgr.getJDBCVirtuosoRep(), datasetUri, true);
//        Utils.initVersionsMaps(new File(path + "4-5.txt"), v1, v2, v1, versionPrefix);
//        Set<String> neigh = Utils.fetchNeighbourNodesV1(dmgr.getJDBCVirtuosoRep(), "http://CIDOC/2", "http://www.cidoc-crm.org/cidoc-crm/#E2.Temporal_Entity");
//        for (String n : neigh) {
//            System.out.println(n);
//        }
//        ChangesExplanator explain = new ChangesExplanator(v1, v2, expl, explProp, false);
//        explain.explain(dmgr.getJDBCVirtuosoRep(), Utils.nodesV1.keySet());
//        explain.explain(dmgr.getJDBCVirtuosoRep(), Utils.nodesV2.keySet());
//        explain.explain(dmgr.getJDBCVirtuosoRep(), Utils.readClasses(new File("explanations/CIDOC_classes.txt")));
//        Set<String> classes = Utils.readClasses(new File("input\\CIDOC\\all_classes.txt"));
//        System.out.println(v2);
//        for (String c : classes) {
//            System.out.println(c + "\t" + Utils.fetchClassParticipation(c, v1, dmgr) + "\t" + Utils.fetchClassParticipation(c, v2, dmgr)
//                    + "\t" + Utils.fetchNeighbourParticipation(c, v1, dmgr) + "\t" + Utils.fetchNeighbourParticipation(c, v2, dmgr));
//        }
//        Utils.AnalyzeChanges(prop, datasetUri);

//        SesameVirtRep sesame = new SesameVirtRep(prop);
//        sesame.exportToFile("test.nt", RDFFormat.NTRIPLES, datasetUri + "1/analysis");
//        sesame.terminate();
        long start = System.currentTimeMillis();
        for (String version : dmgr.fetchDatasetVersions().keySet()) {
            System.out.println("--- " + version + " ---");
            VersionAnalysis.storeClassParticipation(dmgr.getJDBCVirtuosoRep(), version);
            VersionAnalysis.storeNeighborParticipation(dmgr.getJDBCVirtuosoRep(), version);
            System.out.println("Time1: " + (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();
            VersionAnalysis.calcSummaryMeasures(dmgr.getJDBCVirtuosoRep(), version);
            System.out.println("Time2: " + (System.currentTimeMillis() - start));
            break;
        }
//        System.out.println(VersionAnalysis.fetchRankdedClasses(dmgr.getJDBCVirtuosoRep(), v1, AnalysisProperties.neighbPart, 5));
        dmgr.terminate();
    }

}
