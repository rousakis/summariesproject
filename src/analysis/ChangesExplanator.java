/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import gr.forth.ics.virtuoso.JDBCVirtuosoRep;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.diachron.detection.exploit.ChangesExploiter;
import org.diachron.detection.utils.Analysis;

/**
 *
 * @author rousakis
 */
public class ChangesExplanator {

    private String v1, v2, changesOntology, changesOntologySchema, datasetURI;
    private Properties explProp;
    private ChangesExploiter exploiter;
    private boolean labels;

    public ChangesExplanator(String v1, String v2, ChangesExploiter exploiter, Properties explProp, boolean lab) {
        this.v1 = v1;
        this.v2 = v2;
        this.changesOntology = exploiter.fetchChangesOntology(v1, v2);
        this.changesOntologySchema = exploiter.getChangesOntologySchema();
        this.explProp = explProp;
        this.datasetURI = exploiter.getDatasetUri();
        this.exploiter = exploiter;
        this.labels = lab;
    }

    public List<String> fetchProperties(JDBCVirtuosoRep jdbc, String className, String changeName) throws SQLException {
        String query = "select ?prop from <" + changesOntology + "> from <" + changesOntologySchema + "> where {\n"
                + "?sc <http://www.diachron-fp7.eu/changes/name> '" + changeName + "'.\n"
                + "?dsc a ?sc;\n"
                + "     ?p1 <" + className + ">;\n"
                + "     ?p2 ?prop.\n"
                + "?p2 <http://www.diachron-fp7.eu/changes/name> 'property'\n"
                + "filter (?p1 != ?p2).\n"
                + "filter not exists {\n"
                + "?dsc2 a <http://www.diachron-fp7.eu/changes/add_property>;\n"
                + "      ?p ?prop.\n"
                + "}"
                + "}";
        List<String> properties = new ArrayList<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);
        while (results.next()) {
            properties.add(results.getString(1));
        }
        return properties;
    }

    public List<String> fetchSuperclasses(JDBCVirtuosoRep jdbc, String className, String changeName) throws SQLException {
        String query = "select ?superclass from <" + changesOntology + "> from <" + changesOntologySchema + "> where {\n"
                + "?sc <http://www.diachron-fp7.eu/changes/name> '" + changeName + "'.\n"
                + "?dsc a ?sc;\n"
                + "     ?p1 <" + className + ">;\n"
                + "     ?p2 ?superclass.\n"
                + "?p2 <http://www.diachron-fp7.eu/changes/name> 'superclass'.\n"
                + "filter (?p1 != ?p2).\n"
                + "}";
        List<String> superclasses = new ArrayList<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);
        while (results.next()) {
            superclasses.add(results.getString(1));
        }
        return superclasses;
    }

    public Map<String, String> fetchPropInstObjects(JDBCVirtuosoRep jdbc, String className, String changeName) throws SQLException {
        String query = "select ?prop, ?obj from <" + changesOntology + "> from <" + changesOntologySchema + "> where {\n"
                + "?sc <http://www.diachron-fp7.eu/changes/name> '" + changeName + "'.\n"
                + "?dsc a ?sc;\n"
                + "     ?p1 <" + className + ">;\n"
                + "     ?p2 ?prop;\n"
                + "     ?p3 ?obj.\n"
                + "?p2 <http://www.diachron-fp7.eu/changes/name> 'property'.\n"
                + "?p3 <http://www.diachron-fp7.eu/changes/name> 'object'.\n"
                + "?p1 <http://www.diachron-fp7.eu/changes/name> 'subject'.\n"
                + "}";
        Map<String, String> result = new LinkedHashMap<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);
        while (results.next()) {
            result.put(results.getString(2), results.getString(1));
        }
        return result;
    }

    public Map<String, String> fetchPropInstSubjects(JDBCVirtuosoRep jdbc, String className, String changeName) throws SQLException {
        String query = "select ?prop, ?subj from <" + changesOntology + "> from <" + changesOntologySchema + "> where {\n"
                + "?sc <http://www.diachron-fp7.eu/changes/name> '" + changeName + "'.\n"
                + "?dsc a ?sc;\n"
                + "     ?p1 ?subj;\n"
                + "     ?p2 ?prop;\n"
                + "     ?p3 <" + className + ">.\n"
                + "?p2 <http://www.diachron-fp7.eu/changes/name> 'property'.\n"
                + "?p3 <http://www.diachron-fp7.eu/changes/name> 'object'.\n"
                + "?p1 <http://www.diachron-fp7.eu/changes/name> 'subject'.\n"
                + "}";
        Map<String, String> result = new LinkedHashMap<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);
        while (results.next()) {
            result.put(results.getString(2), results.getString(1));
        }
        return result;
    }

    public List<String> fetchSubclasses(JDBCVirtuosoRep jdbc, String className, String changeName) throws SQLException {
        String query = "select ?subclasses from <" + changesOntology + "> from <" + changesOntologySchema + "> where {\n"
                + "?sc <http://www.diachron-fp7.eu/changes/name> '" + changeName + "'.\n"
                + "?dsc a ?sc;\n"
                + "     ?p1 ?subclasses;\n"
                + "     ?p2 <" + className + ">.\n"
                + "?p1 <http://www.diachron-fp7.eu/changes/name> 'subclass'.\n"
                + "filter (?p1 != ?p2).\n"
                + "}";
        List<String> subclasses = new ArrayList<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);
        while (results.next()) {
            subclasses.add(results.getString(1));
        }
        return subclasses;
    }

    public List<String> fetchComplexChangeAnal(JDBCVirtuosoRep jdbc, String className, String changeName) throws SQLException {
        String query = "select ?prop ?param from <" + changesOntology + "> from <" + changesOntologySchema + "> where {\n"
                + "?sc <http://www.diachron-fp7.eu/changes/name> '" + changeName + "'.\n"
                + "?dsc a ?sc;\n"
                + "     ?p1 ?prop;\n"
                + "     ?p2 <" + className + ">.\n"
                + "?p1 <http://www.diachron-fp7.eu/changes/name> 'property'.\n"
                + "?p2 <http://www.diachron-fp7.eu/changes/name> ?param\n"
                + "}";
        List<String> result = new ArrayList<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);

        while (results.next()) {
            result.add("\t\t" + toURI(results.getString(1)) + " (as " + results.getString(2) + ")");
        }
        return result;
    }

    public void explain(JDBCVirtuosoRep jdbc, Set<String> nodes) throws Exception {
        List<String> results;
        Map<String, String> resMap;
        List<String> explanations = new ArrayList<>();
        StringBuilder expl = new StringBuilder();
        StringBuilder classes = new StringBuilder();
        for (String cl : nodes) {

            HashMap<String, Long> changesMap = Analysis.analyzeChangesContainValue(jdbc, changesOntologySchema, datasetURI,
                    v1, v2, null, false, cl);
            String label = "";
            if (labels) {
                label = " - " + getLabel(cl, jdbc);
            }
            expl.append(cl + label + "\n");
            classes.append(cl + label + "\n");
            explanations.clear();
            String classChange = null;
            if (changesMap.containsKey("DELETE_TYPE_CLASS")) {
                expl.append("\t" + (String) explProp.get("DELETE_TYPE_CLASS") + "\n");
                expl.append("-------------------------------------------------" + "\n");
                continue;
            }

            if (changesMap.containsKey("ADD_LABEL") && changesMap.containsKey("DELETE_LABEL")) {
                explanations.add("\tchanged label");
                changesMap.remove("ADD_LABEL");
                changesMap.remove("DELETE_LABEL");
            }
            if (changesMap.containsKey("ADD_COMMENT") && changesMap.containsKey("DELETE_COMMENT")) {
                explanations.add("\tchanged comment");
                changesMap.remove("ADD_COMMENT");
                changesMap.remove("DELETE_COMMENT");
            }

            for (String ch : changesMap.keySet()) {
                if (ch.contains("_RANGE") || ch.contains("_DOMAIN")) {
                    results = fetchProperties(jdbc, cl, ch);
                    for (String property : results) {
                        String explanation = (String) (explProp.get(ch));
                        explanations.add("\t" + explanation.replace("[prop]", "\t\t" + toURI(property)));
                    }
                } else if (ch.contains("_SUPERCLASS")) {
                    results = fetchSubclasses(jdbc, cl, ch);
                    for (String subcl : results) {
                        String explanation = (String) (explProp.get(ch));
                        explanations.add("\t" + explanation.replace("[relation]", "superclass").replace("[class]", "\t\t" + toURI(subcl)));
                    }
                    results = fetchSuperclasses(jdbc, cl, ch);
                    for (String supercl : results) {
                        String explanation = (String) (explProp.get(ch));
                        explanations.add("\t" + explanation.replace("[relation]", "subclass").replace("[class]", "\t\t" + toURI(supercl)));
                    }
                } else if (ch.contains("_PROPERTY_INSTANCE")) {
                    resMap = fetchPropInstObjects(jdbc, cl, ch);
                    String explanation = (String) (explProp.get(ch));
                    for (String obj : resMap.keySet()) {
                        String prop = toURI(resMap.get(obj));
                        if (obj.startsWith("http://")) {
                            obj = toURI(obj);
                        } else {
                            obj = toStringLiteral(obj);
                        }
                        explanations.add("\t" + explanation.replace("[object]", "\t\t" + obj).replace("[property]", "\t\t" + prop));
                    }
                    resMap = fetchPropInstSubjects(jdbc, cl, ch);
                    for (String obj : resMap.keySet()) {
                        String prop = toURI(resMap.get(obj));
                        if (obj.startsWith("http://") || obj.startsWith("_:node")) {
                            obj = toURI(obj);
                        } else {
                            obj = toStringLiteral(obj);
                        }
                        explanations.add("\t" + explanation.replace("[object]", "\t\t" + obj).replace("[property]", "\t\t" + prop));
                    }

                } else if (ch.contains("_property")) {
                    results = fetchComplexChangeAnal(jdbc, cl, ch);
                    String explanation = (String) (explProp.get(ch));
                    for (String result1 : results) {
                        explanations.add("\t" + explanation + result1);
                    }
                } else if (ch.contains("_TYPE_CLASS")) {
                    classChange = (String) explProp.get(ch);
                } else {
                    String explanation = (String) (explProp.get(ch));
                    explanations.add("\t" + explanation);
                }
            }
            if (classChange != null) {
                expl.append("\t" + classChange + "\n");
            }
            for (String explan : explanations) {
                expl.append(explan + "\n");
            }
            /*
             System.out.println("\nThe following classes are connected with: " + toURI(cl) + " \nand have undergone "
             + "some changes as well: ");
             Set<String> neighbours = Utils.fetchNeighbourNodesV2(jdbc, v1, cl);
             neighbours.addAll(fetchNeighbourNodesV2(jdbc, v2, cl));
             for (String neigh : neighbours) {
             System.out.println("\t" + toURI(neigh));
             }*/
            expl.append("-------------------------------------------------" + "\n");
        }
        BufferedWriter bw = new BufferedWriter(new FileWriter("result.txt"));
        bw.write(classes.toString()+"\n\n\n");
        bw.append(expl.toString());
        bw.close();
    }

    public String getLabel(String cl, JDBCVirtuosoRep jdbc) throws SQLException {
        String query = "select ?label from <" + v1 + "> from <" + v2 + "> where {"
                + "<" + cl + "> rdfs:label ?label. }";
        ResultSet res = jdbc.executeSparqlQuery(query, false);
        String label = "";
        while (res.next()) {
            label = res.getString(1);
        }
        if (label.equals("")) {
            label = cl;
        }
        return label;
    }

    public String toURI(String cl) {
        if (labels) {
            try {
                cl = getLabel(cl, exploiter.getRep());
            } catch (SQLException ex) {
                Logger.getLogger(ChangesExplanator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "<" + cl + ">";
    }

    public String toStringLiteral(String cl) {
        return "\"" + cl + "\"";
    }

}
