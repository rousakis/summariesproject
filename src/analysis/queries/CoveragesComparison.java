/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.queries;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author rousakis
 */
public class CoveragesComparison {

    public static void main(String[] args) throws Exception {
        String folder = "USEWOD2014\\DBpedia\\";
        BufferedReader br = new BufferedReader(new FileReader(folder + "dbpedia3.8.txt"));
        String line;
        Map<String, Integer> freqMapv1 = new HashMap<String, Integer>();
        Map<String, Integer> orderMapv1 = new HashMap<String, Integer>();
        int cnt = 1;
        while ((line = br.readLine()) != null) {
            String[] tokens = line.split("\t");
            freqMapv1.put(tokens[0], Integer.parseInt(tokens[1]));
            orderMapv1.put(tokens[0], cnt++);
        }
        br.close();
        br = new BufferedReader(new FileReader(folder + "dbpedia3.9.txt"));
        Map<String, Integer> freqMapv2 = new HashMap<String, Integer>();
        Map<String, Integer> orderMapv2 = new HashMap<String, Integer>();
        cnt = 1;
        while ((line = br.readLine()) != null) {
            String[] tokens = line.split("\t");
            freqMapv2.put(tokens[0], Integer.parseInt(tokens[1]));
            orderMapv2.put(tokens[0], cnt++);
        }
        br.close();

        Set<String> classes = new HashSet<String>(freqMapv1.keySet());
        classes.addAll(freqMapv2.keySet());
        for (String cl : classes) {
            System.out.println(cl + "\t" + freqMapv1.get(cl) + "\t" + freqMapv2.get(cl) + "\t" + orderMapv1.get(cl) + "\t" + orderMapv2.get(cl));
        }

    }

}
