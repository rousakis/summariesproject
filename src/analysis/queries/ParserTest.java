/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.queries;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import org.openrdf.query.algebra.StatementPattern;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.helpers.StatementPatternCollector;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.QueryParser;
import org.openrdf.query.parser.sparql.SPARQLParserFactory;

/**
 *
 * @author rousakis
 */
public class ParserTest {

    public static void main(String[] args) throws Exception {
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        SPARQLParserFactory factory = new SPARQLParserFactory();
        QueryParser parser = factory.getParser();
        ParsedQuery parsedQuery = parser.parseQuery(""
                + "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n"
                + "PREFIX dbpprop: <http://dbpedia.org/property/>\n"
                + "PREFIX dbres: <http://dbpedia.org/resource/>\n"
                + "PREFIX foaf: <http://xmlns.com/foaf/0.1>\n"
                + "\n"
                + "SELECT $team ?nflid $wiki from <http://dbpedia3.8> where {\n"
                + " OPTIONAL {dbres:Sam_Shields dbpprop:currentteam ?team .}\n"
                + " dbres:Sam_Shields dbpprop:nfl ?nflid .\n"
                + " $wiki foaf:primaryTopic dbres:Sam_Shields .\n"
                + "}", null);
        StatementPatternCollector collector = new StatementPatternCollector();
        TupleExpr tupleExpr = parsedQuery.getTupleExpr();
        tupleExpr.visit(collector);
        for (StatementPattern pattern : collector.getStatementPatterns()) {
            System.out.println(pattern.getSubjectVar().getValue());
            System.out.println(pattern.getPredicateVar().getValue());
            System.out.println(pattern.getObjectVar().getValue());

        }
    }

}
