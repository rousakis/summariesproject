/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.queries;

import gr.forth.ics.virtuoso.JDBCVirtuosoRep;
import gr.forth.ics.virtuoso.SesameVirtRep;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openrdf.model.Value;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.query.algebra.StatementPattern;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.helpers.StatementPatternCollector;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.QueryParser;
import org.openrdf.query.parser.sparql.SPARQLParserFactory;
import org.openrdf.repository.RepositoryException;

/**
 *
 * @author rousakis
 */
public class Parser {

    private SesameVirtRep sesame;
    private JDBCVirtuosoRep jdbc;
    private Properties prop;
    private Set<String> classes;
    private Map<String, List<String>> properties;
    private String version;
    private static String prefixes = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n"
            + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
            + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n"
            + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
            + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
            + "PREFIX dc: <http://purl.org/dc/elements/1.1/>\n"
            + "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n"
            + "PREFIX dbo: <http://dbpedia.org/ontology/>\n"
            + "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n"
            + "PREFIX dbpprop: <http://dbpedia.org/property/>\n"
            + "PREFIX dbpedia: <http://dbpedia.org/>\n"
            + "PREFIX dcterms: <http://purl.org/dc/terms/>\n"
            + "PREFIX bif: <bif:>\n"
            + "PREFIX dbres: <http://dbpedia.org/resource/>\n"
            + "PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n"
            + "";

    public Parser(Properties prop, String version) {
        try {
            this.prop = prop;
            sesame = new SesameVirtRep(prop, true);
            jdbc = new JDBCVirtuosoRep(prop);
            classes = jdbc.getClasses(version);
            properties = jdbc.getPropsDomainsRanges(version);
            this.version = version;
        } catch (RepositoryException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void terminate() {
        jdbc.terminate();
        sesame.terminate();
    }

    public String changeQuery(String query, String clause) {
        if (!query.contains(clause)) {
            return query;
        }
        int start = query.indexOf(clause) + clause.length();
        int end = query.indexOf("from <");
        if (end != -1) {
            query = query.substring(0, start) + " * " + query.substring(end);
        }
        return query;
    }

    public Set<String> fetchCoveredClasses(String query) throws MalformedQueryException {
        ///
        HashSet<String> result = new HashSet<>();
        SPARQLParserFactory factory = new SPARQLParserFactory();
        QueryParser parser = factory.getParser();
        ParsedQuery parsedQuery = parser.parseQuery(query, null);
        StatementPatternCollector collector = new StatementPatternCollector();
        TupleExpr tupleExpr = parsedQuery.getTupleExpr();
        tupleExpr.visit(collector);
        Set<String> visited = new HashSet<>();
        for (StatementPattern pattern : collector.getStatementPatterns()) {
            try {
                Value subj = pattern.getSubjectVar().getValue();
                Value pred = pattern.getPredicateVar().getValue();
                Value obj = pattern.getObjectVar().getValue();
                extractCoveredClasses(subj, visited, result);
                extractCoveredClasses(pred, visited, result);
                extractCoveredClasses(obj, visited, result);
            } catch (SQLException | QueryEvaluationException | RepositoryException ex) {
                System.out.println(ex.getMessage());
                jdbc = new JDBCVirtuosoRep(prop);
                return result;
            }
        }
        return result;
    }

    public String fixQuery(String query) {
        if (!query.toLowerCase().contains("prefix")) {
            query = prefixes + "\n" + query;
        }
        if (query.toLowerCase().contains("construct")) {
            return query;
        }
        ///
        query = changeQuery(query, "select");
        query = changeQuery(query, "SELECT");
        query = changeQuery(query, "describe");
        query = changeQuery(query, "DESCRIBE");
        return query;
    }

    private void extractCoveredClasses(Value value, Set<String> visited, HashSet<String> result) throws MalformedQueryException, QueryEvaluationException, RepositoryException, SQLException {
        if (value != null && (value instanceof URIImpl) && !visited.contains(value.stringValue()) && !sesame.belongsInRdfSchema(value.stringValue())) {
            String s = value.stringValue();
            visited.add(s);
            if (classes.contains(s)) {   //the pattern covers a class directly
                result.add(s);
            } else if (properties.containsKey(s)) { //the pattern covers a user defined property; add the domain, range of the property

                for (String obj : properties.get(s)) {
                    if (!sesame.belongsInRdfSchema(obj)) {
                        result.add(obj);
                    }
                }
            } else {
                List<String> types = jdbc.getObjectTypes(s, version);
                for (String type : types) {
                    if (classes.contains(type)) {
                        result.add(type);
                    }
                }
//                String q = "select ?val1, ?val2 from <" + version + "> where {\n"
//                        + "{?val1 ?val2 <" + s + ">.} UNION \n"
//                        + "{?val1 <" + s + "> ?val2.} UNION \n"
//                        + "{<" + s + "> ?val1 ?val2 .} \n "
//                        + "filter (!isLiteral(?val1) && !isLiteral(?val2)).\n"
//                        + "}";
////                TupleQueryResult res = sesame.queryExec(q);
//                ResultSet res = jdbc.executeSparqlQuery(q);
//
//                while (res.next()) {
//                    String val1 = res.getString("val1");
//                    String val2 = res.getString("val2");
//                    if (classes.contains(val1)) {
//                        result.add(val1);
//                    }
//                    if (classes.contains(val2)) {
//                        result.add(val2);
//                    }
//                }
//                res.close();
            }
        }
    }

    public boolean isResourceCovered(String resUri, String query, String version) throws MalformedQueryException, RepositoryException, QueryEvaluationException {
        SPARQLParserFactory factory = new SPARQLParserFactory();
        QueryParser parser = factory.getParser();
        ParsedQuery parsedQuery = parser.parseQuery(query, null);
        StatementPatternCollector collector = new StatementPatternCollector();
        TupleExpr tupleExpr = parsedQuery.getTupleExpr();
        tupleExpr.visit(collector);
        for (StatementPattern pattern : collector.getStatementPatterns()) {
            if (isDirectlyCovered(pattern, resUri)) {
                return true;
            }
            if (isIndirectlyCovered(pattern, resUri, version)) {
                return true;
            }

        }
        return false;
    }

    private boolean isIndirectlyCovered(StatementPattern pattern, String resUri, String version) throws RepositoryException, MalformedQueryException, QueryEvaluationException {
        String v1, v2;
        Value subj = pattern.getSubjectVar().getValue();
        if (subj != null && (subj instanceof URIImpl)) {
            v1 = subj.stringValue();
            v2 = resUri;
            if (pairExists(version, v1, v2) || pairExists(version, v2, v1)) {
                return true;
            }
        }
        Value pred = pattern.getPredicateVar().getValue();
        if (pred != null && (pred instanceof URIImpl)) {
            v1 = pred.stringValue();
            v2 = resUri;
            if (pairExists(version, v1, v2) || pairExists(version, v2, v1)) {
                return true;
            }
        }
        Value obj = pattern.getPredicateVar().getValue();
        if (obj != null && (obj instanceof URIImpl)) {
            v1 = obj.stringValue();
            v2 = resUri;
            if (pairExists(version, v1, v2) || pairExists(version, v2, v1)) {
                return true;
            }
        }
        return false;
    }

    private boolean pairExists(String version, String v1, String v2) throws MalformedQueryException, RepositoryException, QueryEvaluationException {
        String q = "select * from <" + version + "> where {\n"
                + "{?val <" + v1 + "> <" + v2 + ">.} UNION \n"
                + "{<" + v1 + "> ?val <" + v2 + ">.} UNION \n"
                + "{<" + v1 + "> <" + v2 + "> ?val.} \n }";
        TupleQueryResult result = sesame.queryExec(q);
        return result.hasNext();
    }

    private boolean isDirectlyCovered(StatementPattern pattern, String resUri) {
        Value subj = pattern.getSubjectVar().getValue();
        if (subj != null && subj.stringValue().equals(resUri)) {
            return true;
        }
        Value pred = pattern.getPredicateVar().getValue();
        if (pred != null && pred.stringValue().equals(resUri)) {
            return true;
        }
        Value obj = pattern.getObjectVar().getValue();
        if (obj != null && obj.stringValue().equals(resUri)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        String version = "http://dbpedia3.9";
        Parser parser = new Parser(prop, version);
        String folder = "USEWOD2014/DBpedia/dbpedia3.9/dbpedia3.9-correct/";
        HashMap<String, Integer> coverages = new HashMap<>();
        long start = System.currentTimeMillis();
        int queryCnt = 0;
        for (File filename : new File(folder).listFiles()) {
            BufferedReader br = new BufferedReader(new FileReader(filename));
//        BufferedReader br = new BufferedReader(new FileReader(folder + "correct_http07082013.log"));
            StringBuilder sparql = new StringBuilder();
            String line, query = null;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("----")) {
//                    System.out.println(sparql);
//                    System.out.println(parser.isResourceCovered("http://schema.org/CollegeOrUniversity", sparql.toString(), version));
                    Set<String> res;
                    try {
                        query = parser.fixQuery(sparql.toString());
                        res = parser.fetchCoveredClasses(query);
                        for (String cl : res) {
                            if (coverages.containsKey(cl)) {
                                coverages.put(cl, coverages.get(cl) + 1);
                            } else {
                                coverages.put(cl, 1);
                            }
                        }
                        queryCnt++;
                    } catch (Exception ex) {
                        System.out.println("----\nException in: " + query + "----");
                        System.out.println(ex.getMessage() + "\n----");
                        ;
                    }
                    sparql.setLength(0);
//                    System.out.println("#query: " + queryCnt);
                } else {
                    sparql.append(line + "\n");
                }
            }
//            break;
        }
        parser.terminate();
        for (Object c : coverages.keySet()) {
            System.out.println(c + "\t" + coverages.get(c));
        }
        System.out.println("Duration: " + (System.currentTimeMillis() - start) / 1000 + " secs");
        System.out.println("Queries Cnt: " + queryCnt);
    }
}
