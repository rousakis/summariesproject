/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.queries;

import gr.forth.ics.virtuoso.SesameVirtRep;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Properties;

/**
 *
 * @author rousakis
 */
public class DBpediaLogs {

    public static int stringOccur(String str, String findStr) {
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = str.toLowerCase().indexOf(findStr.toLowerCase(), lastIndex);
            if (lastIndex != -1) {
                count++;
                lastIndex += findStr.length();
            }
        }
        return count;
    }

    public static void main(String[] args) throws Exception {
        String folder = "USEWOD2014/DBpedia/dbpedia3.9/";
        String graph = "http://dbpedia3.9";
//        StringBuilder sb = new StringBuilder();
        String s1 = "&query=";
        String s2 = "&format=";
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        for (String filename : new File(folder).list()) {
            if (filename.startsWith(".") || new File(folder + filename).isDirectory() 
                    || filename.startsWith("correct")
                    ) {
                continue;
            }
            BufferedReader br = new BufferedReader(new FileReader(folder + filename));
            BufferedWriter bw = new BufferedWriter(new FileWriter(folder + "correct_" + filename, true));
            String uri;
            System.out.println("--- " + filename + " ---");
            int queryCnt = 1;
            while ((uri = br.readLine()) != null) {
                if (uri.contains(s1)) {
                    String first = uri.substring(uri.indexOf(s1) + s1.length());
                    int end = first.indexOf("&");
                    if (end < 0) {
                        end = first.indexOf(" ");
                    }
                    String query = first.substring(0, end);
                    String queryDec = "";
                    try {
                        SesameVirtRep sesame = new SesameVirtRep(prop, true);
                        queryDec = URLDecoder.decode(query, "UTF-8");
                        queryDec = queryDec.replaceAll("where", "from <" + graph + "> where");
                        queryDec = queryDec.replaceAll("WHERE", "from <" + graph + "> where");
//                    String queryDec2 = "";
//                    if (queryDec.contains("LIMIT") || queryDec.contains("limit")) {
//                        int pos = queryDec.lastIndexOf("}") + 2;
//                        queryDec2 = queryDec.substring(0, pos) + "LIMIT 1";
//                    } else {
//                        queryDec2 = queryDec + " LIMIT 1";
//                    }
//                    sb.append(queryDec + "\n-----------------\n");
                        sesame.queryExec(queryDec);
//                    jdbc.executeSparqlQuery(queryDec);
                        bw.append(queryDec + "\n-----------------\n");
                        sesame.terminate();
                    } catch (Exception ex3) {
//                    System.out.println(queryDec);;
                        System.out.println(ex3.getMessage());
                    }
//                if (queryCnt % threshold == 0) {
//                    bw.append(sb.toString());
//                    sb.setLength(0);
//                }
                }
                System.out.println("[" + filename + "] : " + queryCnt);
                queryCnt++;
            }
//            bw.append(sb.toString());
//            sb.setLength(0);
            br.close();
            bw.close();
        }
    }
}
