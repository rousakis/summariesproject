/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;
import static analysis.utils.Utils.parseFolder;

/**
 *
 * @author rousakis
 */
public class MusicOntology {

    private static void InsertVersions(Properties prop, String musicOntology, String v) throws Exception {
        DatasetsManager mgr = new DatasetsManager(prop, musicOntology);
        mgr.insertDataset(musicOntology, "music ontology", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion("input/MusicOntology/MO_7.8.2007.rdfs", RDFFormat.RDFXML, v + "1", "7-8-2007", musicOntology);
        mgr.insertDatasetVersion("input/MusicOntology/MO_8.8.2007.rdfs", RDFFormat.RDFXML, v + "2", "8-8-2007", musicOntology);
        mgr.insertDatasetVersion("input/MusicOntology/MO_10.08.2007.rdfs", RDFFormat.RDFXML, v + "3", "10-8-2007", musicOntology);
        mgr.insertDatasetVersion("input/MusicOntology/MO_18.9.2007.rdfs", RDFFormat.RDFXML, v + "4", "18-9-2007", musicOntology);
        mgr.insertDatasetVersion("input/MusicOntology/MO_6.12.2007.rdfs", RDFFormat.RDFXML, v + "5", "6-12-2007", musicOntology);
        mgr.insertDatasetVersion("input/MusicOntology/MO_28.7.2008.rdfs", RDFFormat.RDFXML, v + "6", "28-7-2008", musicOntology);
        mgr.insertDatasetVersion("input/MusicOntology/MO_28.10.2008.rdfs", RDFFormat.RDFXML, v + "7", "28-10-2008", musicOntology);
        mgr.insertDatasetVersion("input/MusicOntology/MO_13.02.2010.rdfs", RDFFormat.RDFXML, v + "8", "13-2-2010", musicOntology);
        mgr.terminate();
        //////////
    }

    public static void main(String[] args) throws Exception {
        String musicOntology = "http://music-ontology";
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
//        InsertVersions(prop, musicOntology, v);
//        MCDUtils mcd = new MCDUtils(prop, musicOntology, false);
//        mcd.detectDatasets(false);
//        mcd.terminate();
        ///
        DatasetsManager dmgr = new DatasetsManager(prop, musicOntology);
//        Utils.correctClassesType(dmgr);

        File folder = new File("input/MusicOntology/URIs_all_versions_MO");
        String versionPrefix = "http://www.music-ontology.eu/";
        parseFolder(folder, dmgr, versionPrefix);
        dmgr.terminate();
        ////////
    }
}
