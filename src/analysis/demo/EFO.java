/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.demo;

import analysis.measures.VersionAnalysis;
import analysis.utils.Utils;
import gr.forth.ics.virtuoso.SesameVirtRep;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.MCDUtils;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class EFO {

    private static void InsertVersions(DatasetsManager mgr, String v) throws Exception {
        mgr.insertDataset(mgr.getDatasetURI(), "efo", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.46.nt", RDFFormat.NTRIPLES, v + "2.46", "v2.46", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.47.nt", RDFFormat.NTRIPLES, v + "2.47", "v2.47", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.48.nt", RDFFormat.NTRIPLES, v + "2.48", "v2.48", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.49.nt", RDFFormat.NTRIPLES, v + "2.49", "v2.49", mgr.getDatasetURI());
//        mgr.insertDatasetVersion("input/EFO/EFO - 2.691.owl", RDFFormat.RDFXML, v + "2.691", "v2.691", mgr.getDatasetURI());
//        mgr.insertDatasetVersion("input/EFO/EFO - 2.692.owl", RDFFormat.RDFXML, v + "2.692", "v2.692", mgr.getDatasetURI());
        //////////
    }

    public static void main(String[] args) throws Exception {
        String datasetUri = "http://EFO/";
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        inputStream.close();
        DatasetsManager dmgr = new DatasetsManager(prop, "http://GO/guest");
        dmgr.deleteDataset(true, true);
//        InsertVersions(dmgr, datasetUri);
////        ///
//        Utils.correctClassesType(dmgr);
//        Utils.correctPropertiesType(dmgr);
//        Utils.clearChangesOntologies(prop, datasetUri);
//        Utils.DeleteAllDefinedComplexChanges(prop, datasetUri);
//        Utils.DefineComplexChanges(prop, datasetUri, "input\\CIDOC\\");
//        ////       
//        MCDUtils utils = new MCDUtils(prop, datasetUri, false);
//        utils.detectDatasets(false);
//        utils.terminate();
        //////
//        for (String version : dmgr.fetchDatasetVersions().keySet()) {
//            System.out.println("--- " + version + " ---");
//            long start = System.currentTimeMillis();
//            VersionAnalysis.calcClassParticipation(dmgr.getJDBCVirtuosoRep(), version);
//            VersionAnalysis.calcNeighborParticipation(dmgr.getJDBCVirtuosoRep(), version);
//            System.out.println("Time1: " + (System.currentTimeMillis() - start));
//            start = System.currentTimeMillis();
//            VersionAnalysis.calcSummaryMeasures(dmgr.getJDBCVirtuosoRep(), version);
//            System.out.println("Time2: " + (System.currentTimeMillis() - start));
//            break;
//        }
        
        
        dmgr.terminate();

    }
}
