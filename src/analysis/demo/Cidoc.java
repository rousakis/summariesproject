/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.demo;

import analysis.measures.EvolutionAnalysis;
import analysis.measures.VersionAnalysis;
import analysis.utils.Utils;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class Cidoc {

    private static void InsertVersions(Properties prop, String dataset, String v) throws Exception {
        DatasetsManager mgr = new DatasetsManager(prop, dataset);
        mgr.insertDataset(dataset, "cidoc crm", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_v3.2.1.rdfs", RDFFormat.RDFXML, v + "1", "v3.2.1", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_v3.3.2.rdfs", RDFFormat.RDFXML, v + "2", "v3.3.2", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_v3.4.9.rdfs", RDFFormat.RDFXML, v + "3", "v3.4.9", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_crm_v5.1-draft-2014March.rdfs", RDFFormat.RDFXML, v + "4", "v5.1", dataset);
        mgr.insertDatasetVersion("input/CIDOC/cidoc_crm_v6.2-draft-2015August.rdfs", RDFFormat.RDFXML, v + "5", "v6.2", dataset);
        mgr.terminate();
        //////////
    }

    public static void main(String[] args) throws Exception {
        String datasetUri = "http://CIDOC/";
        Properties prop = new Properties();
        Properties explProp = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        inputStream = new FileInputStream("explanations/explanations.properties");
        explProp.load(inputStream);
        inputStream.close();
        DatasetsManager dmgr = new DatasetsManager(prop, datasetUri);
//        dmgr.deleteDataset(true, true);
//        InsertVersions(prop, datasetUri, datasetUri);
////        ////
//        Utils.clearChangesOntologies(prop, datasetUri);
//        Utils.DeleteAllDefinedComplexChanges(prop, datasetUri);
//        Utils.DefineComplexChanges(prop, datasetUri, "input\\CIDOC\\");
//////       
//        MCDUtils utils = new MCDUtils(prop, datasetUri, false);
//        utils.detectDatasets(false);
//        utils.terminate();

        String v1 = datasetUri + "1", v2 = datasetUri + "2";
        List<String> versions = new ArrayList<>(dmgr.fetchDatasetVersions().keySet());
        for (String version : versions) {
            Utils.correctClassesType(dmgr.getJDBCVirtuosoRep(), version);
            Utils.correctPropertiesType(dmgr.getJDBCVirtuosoRep(), version);
            VersionAnalysis.StoreVersionMeasures(version, dmgr);
        }
        for (int i = 1; i < versions.size(); i++) {
            v1 = versions.get(i - 1);
            v2 = versions.get(i);
            EvolutionAnalysis.calcEvolutionMeasures(dmgr, v1, v2);
        }

        //// gia to ranking
//       System.out.println(VersionAnalysis.fetchRankdedClasses(dmgr.getJDBCVirtuosoRep(), "http://CIDOC/1", AnalysisProperties.classPart, 10));
        ////
//        System.out.println(EvolutionAnalysis.fetchRankdedClasses(dmgr, v1, v2, AnalysisProperties.classChanges, 5));
        dmgr.terminate();
    }
}
