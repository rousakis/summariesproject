/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.demo;

import analysis.measures.AnalysisProperties;
import analysis.measures.EvolutionAnalysis;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class HDOT {

    private static void InsertVersions(DatasetsManager mgr, String v) throws Exception {
        mgr.insertDataset(mgr.getDatasetURI(), "HDOT", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion("input/HDOT/hdot_core_0213.owl", RDFFormat.RDFXML, v + "1", "0213", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/HDOT/hdot_core_1113.owl", RDFFormat.RDFXML, v + "2", "1113", mgr.getDatasetURI());
        //////////
    }

    public static void main(String[] args) throws Exception {
        String datasetUri = "http://HDOT/";
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        inputStream.close();
        DatasetsManager dmgr = new DatasetsManager(prop, datasetUri);
//        dmgr.deleteDataset(true, true);
//        InsertVersions(dmgr, datasetUri);
////        ///
        String v1 = datasetUri + "1";
        String v2 = datasetUri + "2";
//        Utils.clearChangesOntologies(prop, datasetUri);
//        Utils.DeleteAllDefinedComplexChanges(prop, datasetUri);
//        Utils.DefineComplexChanges(prop, datasetUri, "input\\CIDOC\\");
//////       
//        MCDUtils utils = new MCDUtils(prop, datasetUri, false);
//        utils.detectDatasets(false);
//        utils.terminate();
//
        List<String> versions = new ArrayList<>(dmgr.fetchDatasetVersions().keySet());
//        for (String version : versions) {
//            Utils.correctClassesType(dmgr.getJDBCVirtuosoRep(), version);
//            Utils.correctPropertiesType(dmgr.getJDBCVirtuosoRep(), version);
//            VersionAnalysis.CalcVersionMeasures(version, dmgr);
//        }
        
         for (int i = 1; i < versions.size(); i++) {
            v1 = versions.get(i - 1);
            v2 = versions.get(i);
            EvolutionAnalysis.calcEvolutionMeasures(dmgr, v1, v2);
        }
//        System.out.println(EvolutionAnalysis.fetchRankdedClasses(dmgr, v1, v2, AnalysisProperties.Drelevance, 5));
        dmgr.terminate();
    }
}
