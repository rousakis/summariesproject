/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.demo;

import analysis.measures.AnalysisProperties;
import analysis.measures.EvolutionAnalysis;
import analysis.measures.VersionAnalysis;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.diachron.detection.utils.DatasetsManager;

/**
 *
 * @author rousakis
 */
public class DBpedia {

    public static void main(String[] args) throws Exception {
        String datasetUri = "http://dbpedia/";
        Properties prop = new Properties();
        Properties explProp = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        inputStream = new FileInputStream("explanations/explanations.properties");
        explProp.load(inputStream);
        inputStream.close();
        DatasetsManager dmgr = new DatasetsManager(prop, datasetUri);
        List<String> versions = new ArrayList<>(dmgr.fetchDatasetVersions().keySet());
//        for (String version : versions) {
//            VersionAnalysis.StoreVersionMeasures(version, dmgr);
//        }
//        for (int i = 1; i < versions.size(); i++) {
//            String v1 = versions.get(i - 1);
//            String v2 = versions.get(i);
//            EvolutionAnalysis.calcEvolutionMeasures(dmgr, v1, v2);
//        }

//        dmgr.getSesameVirtRep().exportToFile("v1_analysis.nt", RDFFormat.NTRIPLES, "http://dbpedia3.8/analysis");
//        dmgr.getSesameVirtRep().exportToFile("v2_analysis.nt", RDFFormat.NTRIPLES, "http://dbpedia3.9/analysis");
        String v1 = "http://dbpedia3.8";
        String v2 = "http://dbpedia3.9";
//        Map<String, Double> cp1 = VersionAnalysis.fetchRankedClasses(dmgr.getJDBCVirtuosoRep(), v1, AnalysisProperties.classPart, 0);
//        Map<String, Double> np1 = VersionAnalysis.fetchRankedClasses(dmgr.getJDBCVirtuosoRep(), v1, AnalysisProperties.neighbPart, 0);
//        Map<String, Double> cp2 = VersionAnalysis.fetchRankedClasses(dmgr.getJDBCVirtuosoRep(), v2, AnalysisProperties.classPart, 0);
//        Map<String, Double> np2 = VersionAnalysis.fetchRankedClasses(dmgr.getJDBCVirtuosoRep(), v2, AnalysisProperties.neighbPart, 0);
//        Set<String> classes = new HashSet<String>(cp1.keySet());
//        classes.addAll(cp2.keySet());
//        for (String cl : classes) {
//            System.out.println(cl + "\t" + cp1.get(cl) + "\t" + cp2.get(cl) + "\t" + np1.get(cl) + "\t" + np2.get(cl));
//        }

        Map<String, Double> cc1 = EvolutionAnalysis.fetchRankdedClasses(dmgr, v1, v2, AnalysisProperties.classChanges, 0);
        Map<String, Double> cc2 = EvolutionAnalysis.fetchRankdedClasses(dmgr, v1, v2, AnalysisProperties.neighbChanges, 0);
        Set<String> classes = new HashSet<String>(cc1.keySet());
        classes.addAll(cc2.keySet());
        for (String cl : classes) {
            System.out.println(cl + "\t" + cc1.get(cl) + "\t" + cc2.get(cl));
        }

        dmgr.terminate();
    }
}
