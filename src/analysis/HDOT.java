/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import analysis.measures.VersionAnalysis;
import analysis.utils.Utils;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TreeMap;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class HDOT {

    private static void InsertVersions(DatasetsManager mgr, String v) throws Exception {
        mgr.insertDataset(mgr.getDatasetURI(), "HDOT", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion("input/HDOT/hdot_core_0213.owl", RDFFormat.RDFXML, v + "1", "0213", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/HDOT/hdot_core_1113.owl", RDFFormat.RDFXML, v + "2", "1113", mgr.getDatasetURI());
        //////////
    }

    public static void main(String[] args) throws Exception {
        String datasetUri = "http://HDOT/";
        Properties prop = new Properties();
        Properties explProp = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        inputStream = new FileInputStream("explanations/explanations.properties");
        explProp.load(inputStream);
        inputStream.close();
        DatasetsManager dmgr = new DatasetsManager(prop, datasetUri);
//        dmgr.deleteDataset(true, true);
//        InsertVersions(dmgr, datasetUri);
        String v1 = datasetUri + "1";
        String v2 = datasetUri + "2";
        ///
//        String path = "input\\EFO\\URIs_all_versions_CIDOC\\";
//        File folder = new File(path);
        String versionPrefix = datasetUri;
//        Utils.clearChangesOntologies(prop, datasetUri);
//        Utils.DeleteAllDefinedComplexChanges(prop, datasetUri);
//        Utils.DefineComplexChanges(prop, datasetUri, "input\\EFO\\");
        ////
//        MCDUtils utils = new MCDUtils(prop, datasetUri, false);
//        utils.detectDatasets(false);
//        utils.terminate();
        String ver1 = "1";
        String ver2 = "2";
//        createCSVFiles(dmgr, v1, v2, ver1, ver2);
//        Set<String> classes = Utils.readClasses(new File("input\\HDOT\\all_classes_2.txt"));
//        Utils.exportClassData("input/HDOT/" + ver1 + "_" + ver1 + "-" + ver2 + ".csv", classes, dmgr, v1, v2);

//        Utils.AnalyzeChanges(prop, datasetUri);
//        ChangesExploiter expl = new ChangesExploiter(dmgr.getJDBCVirtuosoRep(), datasetUri, true);
//        ChangesExplanator explain = new ChangesExplanator(v1, v2, expl, explProp, true);
//        explain.explain(dmgr.getJDBCVirtuosoRep(), Utils.readClasses(new File("explanations/HDOT_classes.txt")));
//        Set<String> classes = Utils.readClasses(new File("input\\HDOT\\all_classes.txt"));
//        for (String c : classes) {
//            System.out.println(c + "\t" + Utils.fetchClassParticipation(c, v1, dmgr.getJDBCVirtuosoRep()) + "\t" + Utils.fetchClassParticipation(c, v2, dmgr.getJDBCVirtuosoRep())
//                    + "\t" + Utils.fetchNeighbourParticipation(c, v1, dmgr.getJDBCVirtuosoRep()) + "\t" + Utils.fetchNeighbourParticipation(c, v2, dmgr.getJDBCVirtuosoRep()));
//        }
//        Utils.correctClassesType(dmgr);
//        Utils.correctPropertiesType(dmgr);
        for (String version : dmgr.fetchDatasetVersions().keySet()) {
            System.out.println("--- " + version + " ---");
            VersionAnalysis.storeClassParticipation(dmgr.getJDBCVirtuosoRep(), version);
            VersionAnalysis.storeNeighborParticipation(dmgr.getJDBCVirtuosoRep(), version);
            VersionAnalysis.calcSummaryMeasures(dmgr.getJDBCVirtuosoRep(), version);
        }
        dmgr.terminate();

    }

    private static void createCSVFiles(DatasetsManager dmgr, String v1, String v2, String ver1, String ver2) throws SQLException, Exception {
        Utils.nodesV1 = loadNodes(v1, dmgr);
        Utils.nodesV2 = loadNodes(v2, dmgr);
        Utils.exportClassData("input/HDOT/" + ver1 + "_" + ver1 + "-" + ver2 + ".csv", Utils.nodesV1.keySet(), dmgr, v1, v2);
        Utils.exportClassData("input/HDOT/" + ver2 + "_" + ver1 + "-" + ver2 + ".csv", Utils.nodesV1.keySet(), dmgr, v1, v2);
    }

    private static TreeMap<String, String> loadNodes(String v1, DatasetsManager dmgr) throws SQLException {
//        Utils.AnalyzeChanges(prop, datasetUri);
        String sparql = "select ?class from <" + v1 + "> where {"
                + "{"
                + "?class a owl:Class."
                + "} UNION {"
                + "?class a rdfs:Class."
                + "}}";
        ResultSet results = dmgr.getJDBCVirtuosoRep().executeSparqlQuery(sparql, false);
        TreeMap<String, String> classes = new TreeMap<>();
        while (results.next()) {
            classes.put(results.getString(1), v1);
        }
        return classes;
    }
}
