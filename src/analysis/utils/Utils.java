/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.utils;

import gr.forth.ics.virtuoso.JDBCVirtuosoRep;
import gr.forth.ics.virtuoso.SesameVirtRep;
import org.diachron.detection.utils.Analysis;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import org.diachron.detection.associations.AssocManager;
import org.diachron.detection.complex_change.CCDefinitionError;
import org.diachron.detection.complex_change.CCManager;
import org.diachron.detection.exploit.ChangesExploiter;
import org.diachron.detection.utils.ChangesManager;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.IOOps;
import org.diachron.detection.utils.JSONMessagesParser;
import org.diachron.detection.utils.SCDUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class Utils {

    public static TreeMap<String, String> nodesV1 = new TreeMap();
    public static TreeMap<String, String> nodesV2 = new TreeMap();

    public static void findChanges(Properties prop, String datasetUri, String v1, String v2) throws Exception {
        String schema = datasetUri + "changes/schema";
        ChangesManager cManager = new ChangesManager(prop, datasetUri, v1, v2, false);
//        AnalyzeChanges(cManager.getJDBC(), schema, datasetUri, v1, v2, null, false);
        cManager.getJDBC().clearGraph(cManager.getChangesOntology(), false);
        AssocManager assoc = new AssocManager(cManager.getJDBC(), datasetUri, true);
        String assocGraph = assoc.createAssocGraph(v1, v2, false);
//        assocGraph = null;
        SCDUtils scd = new SCDUtils(prop, datasetUri, assocGraph);
        scd.customCompareVersions(v1, v2, null, new String[]{});
        AnalyzeChanges(cManager.getJDBC(), schema, datasetUri, v1, v2, null, false);
        cManager.terminate();
    }

    public static void AnalyzeChanges(JDBCVirtuosoRep jdbc, String ontologySchema, String datasetUri, String v1, String v2, String changeType, boolean tempOntology) throws Exception {
        LinkedHashMap analysis = Analysis.analyzeChanges(jdbc, ontologySchema, datasetUri, v1, v2, changeType, tempOntology);
        System.out.println(v1 + " - " + v2);
        for (Object key : analysis.keySet()) {
            System.out.println(key + "\t" + analysis.get(key));
        }
    }

    public static void AnalyzeChangesContainValue(JDBCVirtuosoRep jdbc, String ontologySchema, String datasetUri, String v1, String v2, String changeType, boolean tempOntology, String nodeUri) throws Exception {
        LinkedHashMap analysis = Analysis.analyzeChangesContainValue(jdbc, ontologySchema, datasetUri, v1, v2, changeType, tempOntology, nodeUri);
        System.out.println(v1 + " - " + v2);
        for (Object key : analysis.keySet()) {
            System.out.println(key + "\t" + analysis.get(key));
        }
    }

    public static Set<String> fetchNeighbourNodesV1(JDBCVirtuosoRep jdbc, String version, String node) throws SQLException {
        Set<String> nodes = new LinkedHashSet<>();
        String sparql = "select ?node1 from <" + version + "> where {\n"
                + "{ \n"
                + "?node1 ?p1 <" + node + ">. \n"
                + "} UNION { \n"
                + "<" + node + "> ?p1 ?node1. \n"
                + "}\n"
                + "filter(?node1 != rdfs:Class). \n"
                + "filter(?p1 != rdfs:label && ?p1 != rdfs:comment). \n"
                + "}";
        ResultSet res = jdbc.executeSparqlQuery(sparql.toString(), false);
        while (res.next()) {
            String n1 = res.getString(1);
            if (n1 != null) {
                nodes.add(n1);
            }
        }
        return nodes;
    }

    public static Set<String> fetchNeighbourNodesV2(JDBCVirtuosoRep jdbc, String version, String node) throws SQLException {
        Set<String> nodes = new LinkedHashSet<>();
        String sparql = "select ?node1 from <" + version + "> where {\n"
                + "{ \n"
                + "?node1 rdfs:subClassOf <" + node + ">. \n"
                + "} UNION { \n"
                + "<" + node + "> rdfs:subClassOf ?node1. \n"
                + "}"
                + " UNION { \n"
                + "?p rdfs:domain <" + node + ">.\n"
                + "?p rdfs:range ?node1.\n"
                + "} UNION { \n"
                + "?p rdfs:domain ?node1.\n"
                + "?p rdfs:range <" + node + ">.\n"
                + "}\n"
                + ""
                + "}";
        ResultSet res = jdbc.executeNestedSparqlQuery(sparql.toString(), false);
        while (res.next()) {
            String n1 = res.getString(1);
            if (n1 != null) {
                nodes.add(n1);
            }
        }
        jdbc.terminateNestedStatement();
        return nodes;
    }

    public static void createCSVURIsAnalysis(DatasetsManager dmgr, File inputFile, String versionPrefix) throws Exception {
        String name = inputFile.getName();
        String[] tokens = name.split("-");
        String v1 = tokens[0];
        String v2 = tokens[1].replace(".txt", "");
        String curVersion = v1;
        initVersionsMaps(inputFile, curVersion, v2, v1, versionPrefix);
        exportClassData(inputFile.getParent() + "/" + v1 + "_" + v1 + "-" + v2 + ".csv", nodesV1.keySet(), dmgr, versionPrefix + v1, versionPrefix + v2);
        exportClassData(inputFile.getParent() + "/" + v2 + "_" + v1 + "-" + v2 + ".csv", nodesV2.keySet(), dmgr, versionPrefix + v1, versionPrefix + v2);
    }

    public static void exportClassData(String output, Set<String> versionClasses, DatasetsManager dmgr, String oldV, String newV) throws Exception, IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(output));
        System.out.println("--- Creating file " + output.substring(output.lastIndexOf("/") + 1) + " ---");
        bw.write("\"Class\"\tClass Changes\tNeighbourhood Changes\n");
        Set<String> neighbours;
        for (String node : versionClasses) {
            long classChanges = getChangesNumberPerNodeMaterialized(dmgr, oldV, newV, node);
//            if (classChanges == 0) {
//                continue;
//            }
            neighbours = fetchNeighbourNodesV2(dmgr.getJDBCVirtuosoRep(), oldV, node);
            neighbours.addAll(fetchNeighbourNodesV2(dmgr.getJDBCVirtuosoRep(), newV, node));
            long neighbChanges = 0;
            for (String neighbour : neighbours) {
                neighbChanges += getChangesNumberPerNodeMaterialized(dmgr, oldV, newV, neighbour);
            }
            bw.write("\"" + node + "\"" + "\t" + classChanges + "\t" + neighbChanges + "\n");
        }
        bw.close();
    }

    public static long getChangesNumberPerNodeMaterialized(DatasetsManager dmgr, String v1, String v2, String node) throws Exception {
        long classChanges = 0;
        HashMap<String, Long> changesMap = Analysis.analyzeChangesContainValue(dmgr.getJDBCVirtuosoRep(), dmgr.getChangesSchema(), dmgr.getDatasetURI(), v1, v2, null, false, node);
        for (String ch : changesMap.keySet()) {
            classChanges += changesMap.get(ch);
        }
        return classChanges;
    }

//    public static long getChangesNumberPerNode(ChangesExploiter expl, String v1, String v2, String node) throws Exception {
//        long classChanges = 0;
//        HashMap<String, Long> changesMap = Analysis.analyzeChangesContainValue(expl.getRep(), expl.getChangesOntologySchema(), expl.getDatasetUri(), v1, v2, null, false, node);
//        for (String ch : changesMap.keySet()) {
//            classChanges += changesMap.get(ch);
//        }
//        return classChanges;
//    }

    public static void createJSONURIsAnalysis(DatasetsManager dmgr, File inputFile, String versionPrefix) throws Exception {
        String name = inputFile.getName();
        String[] tokens = name.split("-");
        String v1 = tokens[0];
        String v2 = tokens[1].replace(".txt", "");
        String curVersion = v1;
        initVersionsMaps(inputFile, curVersion, v2, v1, versionPrefix);
        exportClassAnalysisDataAnalysisJSON(inputFile.getParent() + "/" + v1 + "_" + v1 + "-" + v2 + ".json", nodesV1, dmgr, versionPrefix + v1, versionPrefix + v2);
        exportClassAnalysisDataAnalysisJSON(inputFile.getParent() + "/" + v2 + "_" + v1 + "-" + v2 + ".json", nodesV2, dmgr, versionPrefix + v1, versionPrefix + v2);

    }

    static void initVersionsMaps(File inputFile, String curVersion, String v2, String v1, String versionPrefix) throws FileNotFoundException, IOException {
        String line;
        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        while ((line = br.readLine()) != null) {
            if (line.length() < 1) {
                curVersion = v2;
            }
            if (line.isEmpty()) {
                continue;
            }
            if (curVersion.equals(v1)) {
                nodesV1.put(line, versionPrefix + curVersion);
            } else {
                nodesV2.put(line, versionPrefix + curVersion);
            }
        }
        br.close();
    }

    public static Set<String> readClasses(File inputFile) throws IOException {
        LinkedHashSet<String> results = new LinkedHashSet<>();
        String line;
        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        while ((line = br.readLine()) != null) {
            results.add(line);
        }
        br.close();
        return results;
    }

    public static void exportClassAnalysisDataAnalysisJSON(String outputFile, TreeMap<String, String> nodesV1, DatasetsManager dmgr, String v1, String v2) throws IOException, Exception {
        JSONObject jsonFile = new JSONObject();
        JSONObject classesJson = new JSONObject();
        jsonFile.put("classes", classesJson);
        Set<String> neighbours;
        jsonFile.put("changes_num", Analysis.getChangesNum(dmgr, v1, v2));
        for (String node : nodesV1.keySet()) {
            System.out.println("\tANALYZING CLASS ... " + node);
            JSONObject value = createJSONClassAnalElement(dmgr, v1, v2, node);
            neighbours = fetchNeighbourNodesV2(dmgr.getJDBCVirtuosoRep(), v1, node);
            neighbours.addAll(fetchNeighbourNodesV2(dmgr.getJDBCVirtuosoRep(), v2, node));
            JSONArray neighboursJson = new JSONArray();
            JSONObject internalValue;
            value.put("neighbours_sum", neighbours.size());
            for (String neighbour : neighbours) {
                JSONObject internal = createJSONClassAnalElement(dmgr, v1, v2, neighbour);
                if ((long) internal.get("changes_sum") > 0) {
                    internalValue = new JSONObject();
                    internalValue.put(neighbour, internal);
                    neighboursJson.add(internalValue);
                }
            }
            value.put("neighbours", neighboursJson);
            classesJson.put(node, value);
        }
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
        bw.write(jsonFile.toString());
        bw.close();
    }

    private static JSONObject createJSONClassAnalElement(DatasetsManager dmgr, String v1, String v2, String node) throws Exception {
        long sum = 0;
        HashMap<String, Long> changesMap;
        JSONObject value = new JSONObject();
        changesMap = Analysis.analyzeChangesContainValue(dmgr.getJDBCVirtuosoRep(), dmgr.getChangesSchema(), dmgr.getDatasetURI(), v1, v2, null, false, node);
        value.put("changes_analysis", new JSONObject(changesMap));
        for (String ch : changesMap.keySet()) {
            sum += changesMap.get(ch);
        }
        value.put("changes_sum", sum);
        return value;
    }

    public static void parseFolder(File folder, DatasetsManager dmgr, String versionPrefix) throws Exception {
        for (String file : folder.list()) {
            if (file.endsWith(".txt")) {
                System.out.println("-- PARSING FILES ... " + file + " --");
//                Utils.createJSONURIsAnalysis(dmgr, new File(folder.getAbsolutePath() + "\\" + file), versionPrefix);
                Utils.createCSVURIsAnalysis(dmgr, new File(folder.getAbsolutePath() + "\\" + file), versionPrefix);
                System.out.println("-- DONE --");
                break;
            }
        }
    }

    public static void correctClassesType(JDBCVirtuosoRep jdbc, String version) throws SQLException {
        String query = "select distinct ?class from <" + version + "> where {\n"
                + "{?class ?pred ?o.\n"
                + "filter (?pred in (rdfs:subClassOf)).} UNION \n"
                + "{?s ?pred ?class.\n"
                + "filter (?pred in (rdfs:subClassOf, rdfs:domain, rdfs:range)).}\n "
                + "filter not exists {?class a ?type. filter (?type in (rdfs:Class, owl:Class)).} \n"
                + "filter (?class not in (rdfs:Class, rdfs:Resource, rdf:Property, rdfs:Literal)). \n"
                + "filter (!strStarts(str(?class), '_:node')).\n"
                + "}";
        List<String> classes = new ArrayList<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);
        while (results.next()) {
            classes.add(results.getString(1));
        }
        for (String cl : classes) {
            jdbc.addTriple(cl, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "http://www.w3.org/2000/01/rdf-schema#Class", version);
        }
    }

    public static void correctPropertiesType(JDBCVirtuosoRep jdbc, String version) throws SQLException {
        Map<String, String> genericNamespaces = new HashMap<>();
        genericNamespaces.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        genericNamespaces.put("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        genericNamespaces.put("owl", "http://www.w3.org/2002/07/owl#");
        String query = "select distinct ?property from <" + version + "> where {\n"
                + "{?s ?property ?o.\n"
                + "filter ("
                + "!strStarts(str(?property), '" + genericNamespaces.get("rdf") + "') && "
                + "!strStarts(str(?property), '" + genericNamespaces.get("rdfs") + "') && "
                + "!strStarts(str(?property), '" + genericNamespaces.get("owl") + "') ).\n"
                + "} UNION \n"
                + "{"
                + "?property rdfs:domain ?dom.\n"
                + "?property rdfs:range ?ran.\n"
                + "}\n"
                + "filter not exists {?property a ?type. filter (?type in (rdf:Property, owl:ObjectProperty, owl:DatatypeProperty)).} \n"
                + "}";
        List<String> property = new ArrayList<>();
        ResultSet results = jdbc.executeSparqlQuery(query, false);
        while (results.next()) {
            property.add(results.getString(1));
        }
        for (String prop : property) {
            jdbc.addTriple(prop, genericNamespaces.get("rdf") + "type", genericNamespaces.get("rdf") + "Property", version);
        }
    }

    public static void clearChangesOntologies(Properties prop, String datasetUri) throws Exception {
        DatasetsManager dManager = new DatasetsManager(prop, datasetUri);
        dManager.fetchDatasetVersions();
        List<String> versions = new ArrayList(dManager.fetchDatasetVersions().keySet());
        dManager.terminate();
        ChangesManager cManager;
        for (int i = 1; i < versions.size(); i++) {
            String v1 = versions.get(i - 1);
            String v2 = versions.get(i);
            cManager = new ChangesManager(prop, datasetUri, v1, v2, false);
            cManager.deleteChangesOntology();
        }
    }

    public static void DeleteAllDefinedComplexChanges(Properties propFile, String datasetUri) throws Exception {
        ChangesExploiter expl = new ChangesExploiter(propFile, datasetUri, true);
        List<String> ontologies = expl.getChangesOntologies();
        String ontologySchema = expl.getChangesOntologySchema();
        expl.terminate();
        CCManager delete = new CCManager(propFile, ontologySchema);
        for (String ontology : ontologies) {
            delete.deleteAllComplexChanges(ontology, true);
        }
        delete.deleteAllComplexChanges(ontologySchema, false);
        delete.terminate();
    }

    public static void AnalyzeChanges(Properties prop, String datasetUri) throws Exception {
        DatasetsManager dmgr = new DatasetsManager(prop, datasetUri);
        List<String> versions = new ArrayList(dmgr.fetchDatasetVersions().keySet());
        for (int i = 1; i < dmgr.fetchDatasetVersions().size(); i++) {
            String v1 = versions.get(i - 1);
            String v2 = versions.get(i);
            LinkedHashMap analysis = Analysis.analyzeChanges(dmgr.getJDBCVirtuosoRep(), datasetUri + "changes/schema", datasetUri, v1, v2, null, false);
            System.out.println(v1 + " - " + v2);
            for (Object key : analysis.keySet()) {
                System.out.println(key + "\t" + analysis.get(key));
            }
            System.out.println("-----");
        }
        dmgr.terminate();
    }

    public static void DefineComplexChanges(Properties prop, String datasetUri, String folder) throws Exception {
        ChangesExploiter expl = new ChangesExploiter(prop, datasetUri, true);
        String ontologySchema = expl.getChangesOntologySchema();
        expl.terminate();
        String ccFolder;
        List<File> changes = new ArrayList<>();
        ccFolder = folder + "complex changes";
        List<File> f = Arrays.asList(new File(ccFolder).listFiles());
        changes.addAll(f);
        ////
        for (File change : changes) {
            if (!change.getName().startsWith(".") && change.isFile()) {
                String json = IOOps.readData(change.getPath());
                CCManager manager = JSONMessagesParser.createCCDefinition(prop, json, ontologySchema);
                if (manager.getCcDefError().getErrorCode() == null) {
                    CCDefinitionError error = manager.insertChangeDefinition();
                    if (error.getErrorCode() != null) {
                        System.out.println(error);
                        break;
                    }
                } else {
                    System.out.println(manager.getCcDefError());
                    break;
                }
                manager.terminate();
            }
        }
    }

    public static int fetchClassParticipation(String node, String version, JDBCVirtuosoRep jdbc) throws SQLException {
        String query = "select count(*) from <" + version + "> where {\n"
                + "{\n"
                + "<" + node + "> ?p ?o.\n"
                + "} UNION {\n"
                + "?s ?p <" + node + ">.\n"
                + "}\n"
                + "}";
        ResultSet result = jdbc.executeNestedSparqlQuery(query, false);
        while (result.next()) {
            return result.getInt(1);
        }
        jdbc.terminateNestedStatement();
        return 0;
    }

    public static int fetchNeighbourParticipation(String node, String version, JDBCVirtuosoRep jdbc) throws SQLException {
        int sum = 0;
        Set<String> neighbors = fetchNeighbourNodesV2(jdbc, version, node);
        for (String neighbor : neighbors) {
            sum += fetchClassParticipation(neighbor, version, jdbc);
        }
        return sum;
    }

    public static int fetchMainMemoryNeighbourParticipation(String node, String version, JDBCVirtuosoRep jdbc, Map<String, Integer> cp) throws SQLException {
        int sum = 0;
        Set<String> neighbors = fetchNeighbourNodesV2(jdbc, version, node);
        for (String neighbor : neighbors) {
            Integer result = cp.get(neighbor);
            if (result != null) {
                sum += result;
            }
        }
        return sum;
    }

    public static void customCompareAndAnalyze(BufferedWriter bw, String v11, String v21, Properties prop, String datasetURI) throws ClassNotFoundException, SQLException, Exception, IOException {
        SCDUtils scd = new SCDUtils(prop, datasetURI, null);
        scd.customCompareVersions(v11, v21, null, null);
        String v1 = v11;
        String v2 = v21;
        JDBCVirtuosoRep jdbc = new JDBCVirtuosoRep(prop);
        LinkedHashMap analysis = Analysis.analyzeChanges(jdbc, datasetURI + "changes/schema", datasetURI, v1, v2, null, false);
        int addedCnt = 0, deletedCnt = 0;
        bw.append(v1 + "\ttriples: " + jdbc.getTriplesNum(v1) + "\n");
        bw.append(v2 + "\ttriples: " + jdbc.getTriplesNum(v2) + "\n");
        for (Object key : analysis.keySet()) {
            bw.append(key + "\t" + analysis.get(key) + "\n");
            if (((String) key).startsWith("ADD_")) {
                addedCnt += (long) analysis.get(key);
            } else {
                deletedCnt += (long) analysis.get(key);
            }
        }
        bw.append("-----\n");
        bw.append("ADDED_TRIPLES\t" + addedCnt + "\n");
        bw.append("DELETED_TRIPLES\t" + deletedCnt + "\n");
        bw.append("---------------------------------------\n\n");
        jdbc.terminate();

//        dmgr.terminate();
    }

    public static void importTriplesString(StringBuilder triplesBlock, Properties properties, String graphDst) throws RepositoryException, Exception, IOException {
        String filename = System.currentTimeMillis() + ".nt";
        BufferedWriter bw = new BufferedWriter(new FileWriter(filename), 65536);
        bw.write(triplesBlock.toString());
        bw.close();
        SesameVirtRep sesame = new SesameVirtRep(properties, true);
        sesame.importFile(filename, RDFFormat.NTRIPLES, graphDst);
        sesame.terminate();
        new File(filename).delete();
    }

    public static Double truncate(double original) {
        DecimalFormat df = new DecimalFormat("#.#####");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        df.setRoundingMode(RoundingMode.CEILING);
        String result = df.format(original);
        return Double.parseDouble(result);
    }

}
