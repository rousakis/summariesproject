/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import analysis.measures.VersionAnalysis;
import analysis.utils.Utils;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TreeMap;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class EFO {

    private static void InsertVersions(DatasetsManager mgr, String v) throws Exception {
        mgr.insertDataset(mgr.getDatasetURI(), "efo", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.46.nt", RDFFormat.NTRIPLES, v + "2.46", "v2.46", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.47.nt", RDFFormat.NTRIPLES, v + "2.47", "v2.47", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.48.nt", RDFFormat.NTRIPLES, v + "2.48", "v2.48", mgr.getDatasetURI());
        mgr.insertDatasetVersion("input/EFO/_diachron_efo-2.49.nt", RDFFormat.NTRIPLES, v + "2.49", "v2.49", mgr.getDatasetURI());
//        mgr.insertDatasetVersion("input/EFO/EFO - 2.691.owl", RDFFormat.RDFXML, v + "2.691", "v2.691", mgr.getDatasetURI());
//        mgr.insertDatasetVersion("input/EFO/EFO - 2.692.owl", RDFFormat.RDFXML, v + "2.692", "v2.692", mgr.getDatasetURI());
        //////////
    }

    public static void main(String[] args) throws Exception {
        String datasetUri = "http://EFO/";
        Properties prop = new Properties();
        Properties explProp = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        inputStream = new FileInputStream("explanations/explanations.properties");
        explProp.load(inputStream);
        inputStream.close();
        DatasetsManager dmgr = new DatasetsManager(prop, datasetUri);
        String v1 = datasetUri + "2.46";
        String v2 = datasetUri + "2.47";
        String v3 = datasetUri + "2.48";
        String v4 = datasetUri + "2.49";
        String v5 = datasetUri + "2.691";
        String v6 = datasetUri + "2.692";
//        InsertVersions(dmgr, datasetUri);
        ///
//        String path = "input\\EFO\\URIs_all_versions_CIDOC\\";
//        File folder = new File(path);
//        String versionPrefix = datasetUri;
//        Utils.clearChangesOntologies(prop, datasetUri);
//        Utils.DeleteAllDefinedComplexChanges(prop, datasetUri);
//        Utils.DefineComplexChanges(prop, datasetUri, "input\\EFO\\");
        ////
//        MCDUtils utils = new MCDUtils(prop, datasetUri, false);
//        utils.detectDatasets(false);
//        utils.terminate();
        String ver1 = "2.46";
        String ver2 = "2.47";
        String ver3 = "2.48";
        String ver4 = "2.49";
        String ver5 = "2.691";
        String ver6 = "2.692";
//        createCSVFiles(dmgr, v1, v2, ver1, ver2);
//        createCSVFiles(dmgr, v2, v3, ver2, ver3);
//        createCSVFiles(dmgr, v3, v4, ver3, ver4);

//        Utils.AnalyzeChanges(prop, datasetUri);
//        Set<String> classes = Utils.readClasses(new File("input\\EFO\\all_classes.txt"));
//        Utils.exportClassData("input/EFO/" + ver5 + "_" + ver6 + "-" + ver6 + ".csv", classes, dmgr, v5, v6);
//        ChangesExploiter expl = new ChangesExploiter(dmgr.getJDBCVirtuosoRep(), datasetUri, true);
//        ChangesExplanator explain = new ChangesExplanator(v5, v6, expl, explProp,true);
//        explain.explain(dmgr.getJDBCVirtuosoRep(), Utils.readClasses(new File("explanations/EFO_classes_v2.txt")));
//        for (String c : classes) {
//            System.out.println(c + "\t" + Utils.fetchClassParticipation(c, v1, dmgr) + "\t" + Utils.fetchClassParticipation(c, v2, dmgr)
//                    + "\t" + Utils.fetchNeighbourParticipation(c, v1, dmgr) + "\t" + Utils.fetchNeighbourParticipation(c, v2, dmgr));
//        }
//        Utils.correctClassesType(dmgr);
//        Utils.correctPropertiesType(dmgr);
        long start = System.currentTimeMillis();
        for (String version : dmgr.fetchDatasetVersions().keySet()) {
            System.out.println("--- " + version + " ---");
            VersionAnalysis.storeClassParticipation(dmgr.getJDBCVirtuosoRep(), version);
            VersionAnalysis.storeNeighborParticipation(dmgr.getJDBCVirtuosoRep(), version);
            System.out.println("Time1: " + (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();
            VersionAnalysis.calcSummaryMeasures(dmgr.getJDBCVirtuosoRep(), version);
            System.out.println("Time2: " + (System.currentTimeMillis() - start));
            break;
        }
        dmgr.terminate();

    }

    private static void createCSVFiles(DatasetsManager dmgr, String v1, String v2, String ver1, String ver2) throws SQLException, Exception {
        Utils.nodesV1 = loadNodes(v1, dmgr);
        Utils.nodesV2 = loadNodes(v2, dmgr);
        Utils.exportClassData("input/EFO/URIs_all_versions_CIDOC/" + ver1 + "_" + ver1 + "-" + ver2 + ".csv", Utils.nodesV1.keySet(), dmgr, v1, v2);
        Utils.exportClassData("input/EFO/URIs_all_versions_CIDOC/" + ver2 + "_" + ver1 + "-" + ver2 + ".csv", Utils.nodesV1.keySet(), dmgr, v1, v2);
    }

    private static TreeMap<String, String> loadNodes(String v1, DatasetsManager dmgr) throws SQLException {
//        Utils.AnalyzeChanges(prop, datasetUri);
        String sparql = "select ?class from <" + v1 + "> where {"
                + "?class a owl:Class."
                + "}";
        ResultSet results = dmgr.getJDBCVirtuosoRep().executeSparqlQuery(sparql, false);
        TreeMap<String, String> classes = new TreeMap<>();
        while (results.next()) {
            classes.put(results.getString(1), v1);
        }
        return classes;
    }
}
