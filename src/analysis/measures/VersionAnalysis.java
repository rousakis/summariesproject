/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.measures;

import analysis.utils.Utils;
import gr.forth.ics.virtuoso.JDBCVirtuosoRep;
import gr.forth.ics.virtuoso.SesameVirtRep;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.diachron.detection.utils.DatasetsManager;
import org.openrdf.repository.RepositoryException;
import rdfdigest.RDFdigest;

/**
 *
 * @author rousakis
 */
public class VersionAnalysis {

    public static Map<String, Integer> calcClassParticipation(String version, JDBCVirtuosoRep jdbc) throws SQLException {
        Map<String, Integer> cp = new LinkedHashMap<>();
        String query = "select ?c, count(?c) as ?cp from <" + version + "> {\n"
                + "{\n"
                + "?c a ?type.\n"
                + "filter (?type = owl:Class)\n"
                + "{\n"
                + "?c ?p ?o.\n"
                + "} UNION {\n"
                + "?s ?p ?c.\n"
                + "}\n"
                + "filter (!strStarts(str(?c), '_:node')).\n"
                + "} UNION \n"
                + "{\n"
                + "?c a ?type.\n"
                + "filter (?type = rdf:Class)\n"
                + "{\n"
                + "?c ?p ?o.\n"
                + "} UNION {\n"
                + "?s ?p ?c.\n"
                + "}\n"
                + "filter (!strStarts(str(?c), '_:node')).\n"
                + "}\n"
                + "} order by desc(?cp)";
        ResultSet result = jdbc.executeSparqlQuery(query, false);
        while (result.next()) {
            cp.put(result.getString(1), result.getInt(2));
        }
        result.close();
        return cp;
    }

    public static void storeClassParticipation(JDBCVirtuosoRep jdbc, String version) throws Exception {
        jdbc.clearGraph(version + "/analysis", true);
        Map<String, Integer> cp = calcClassParticipation(version, jdbc);
        StringBuilder triplesBlock = new StringBuilder();
        int max = 0;
        for (String cl : cp.keySet()) {
            if (max == 0) {
                max = cp.get(cl);
            }
            triplesBlock.append("<").append(cl).append("> ").
                    append("<" + AnalysisProperties.classPartNorm + ">").
                    append(" \"").append(Utils.truncate((double) cp.get(cl) / (double) max)).append("\"^^<http://www.w3.org/2001/XMLSchema#Double>.\n");
            triplesBlock.append("<").append(cl).append("> ").
                    append("<" + AnalysisProperties.classPart + ">").
                    append(" \"").append(Utils.truncate((double) cp.get(cl))).append("\"^^<http://www.w3.org/2001/XMLSchema#Double>.\n");
        }
        Utils.importTriplesString(triplesBlock, jdbc.getProperties(), version + "/analysis");
    }

    public static Map<String, Integer> getNeighborParticipation(String version, JDBCVirtuosoRep jdbc) throws SQLException {
        Map<String, Integer> np = new HashMap<>();
        Map<String, Integer> cp = calcClassParticipation(version, jdbc);
        for (String c : cp.keySet()) {
            int nbp = Utils.fetchMainMemoryNeighbourParticipation(c, version, jdbc, cp);
            np.put(c, nbp);
        }
        return np;
    }

    public static void storeNeighborParticipation(JDBCVirtuosoRep jdbc, String version) throws Exception {
        Map<String, Integer> cp = calcClassParticipation(version, jdbc);
        StringBuilder triplesBlock = new StringBuilder();
        int max = 0;
        Map<String, Integer> np = new HashMap<>();
        for (String c : cp.keySet()) {
            int nbp = Utils.fetchMainMemoryNeighbourParticipation(c, version, jdbc, cp);
            if (max < nbp) {
                max = nbp;
            }
            np.put(c, nbp);
        }
        for (String cl : np.keySet()) {
            triplesBlock.append("<").append(cl).append("> ").
                    append("<" + AnalysisProperties.neighbPartNorm + ">").
                    append(" \"").append(Utils.truncate((double) np.get(cl) / (double) max)).append("\"^^<http://www.w3.org/2001/XMLSchema#Double>.\n");
            triplesBlock.append("<").append(cl).append("> ").
                    append("<" + AnalysisProperties.neighbPart + ">").
                    append(" \"").append(Utils.truncate((double) np.get(cl))).append("\"^^<http://www.w3.org/2001/XMLSchema#Double>.\n");
        }
        Utils.importTriplesString(triplesBlock, jdbc.getProperties(), version + "/analysis");
    }

    public static void calcSummaryMeasures(JDBCVirtuosoRep jdbc, String v1) throws RepositoryException {
        SesameVirtRep sesame = new SesameVirtRep(jdbc.getProperties(), true);
        RDFdigest rdfDigest = new RDFdigest(sesame.getRepository());
        rdfDigest.updateRDFdigestInfo(v1, v1 + "/analysis");
        sesame.terminate();
    }

    public static Map<String, Double> fetchRankedClasses(JDBCVirtuosoRep jdbc, String version, String analProperty, int top) throws Exception {
        Map<String, Double> results = new LinkedHashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("select ?class, ?value from <" + version + "/analysis> where { \n").
                append("?class <" + analProperty + "> ?value.\n").
                append("} order by desc(?value)");
        if (top > 0) {
            query.append(" limit " + top);
        }
        ResultSet result = jdbc.executeSparqlQuery(query.toString(), false);
        while (result.next()) {
            double cp = result.getDouble(2);
            results.put(result.getString(1), cp);
        }
        result.close();
        return results;
    }

    public static void StoreVersionMeasures(String version, DatasetsManager dmgr) throws Exception, RepositoryException {
        System.out.println("--- " + version + " ---");
        long start = System.currentTimeMillis();
        VersionAnalysis.storeClassParticipation(dmgr.getJDBCVirtuosoRep(), version);
        VersionAnalysis.storeNeighborParticipation(dmgr.getJDBCVirtuosoRep(), version);
        System.out.println("Time1: " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
//        VersionAnalysis.calcSummaryMeasures(dmgr.getJDBCVirtuosoRep(), version);
//        System.out.println("Time2: " + (System.currentTimeMillis() - start));
    }
}
