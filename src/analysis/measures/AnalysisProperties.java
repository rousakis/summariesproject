/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.measures;

/**
 *
 * @author rousakis
 */
public class AnalysisProperties {

    public static final String classPartNorm = "http://ics.forth.gr/isl/metrics/participation/class";
    public static final String neighbPartNorm = "http://ics.forth.gr/isl/metrics/participation/neighborhood";
    public static final String classPart = "http://ics.forth.gr/isl/metrics/participation/raw/class";
    public static final String neighbPart = "http://ics.forth.gr/isl/metrics/participation/raw/neighborhood";
    public static final String relevance = "http://ics.forth.gr/isl/metrics/relevance";
    public static final String cIn = "http://ics.forth.gr/isl/metrics/centrality/in";
    public static final String cOut = "http://ics.forth.gr/isl/metrics/centrality/out";
    //////
    public static final String classChangesNorm = "http://ics.forth.gr/isl/metrics/changes/class";
    public static final String neighbChangesNorm = "http://ics.forth.gr/isl/metrics/changes/neighborhood";
    public static final String classChanges = "http://ics.forth.gr/isl/metrics/changes/raw/class";
    public static final String neighbChanges = "http://ics.forth.gr/isl/metrics/changes/raw/neighborhood";
//    public static final String Dchange = "http://ics.forth.gr/isl/metrics/Dchange";
    public static final String DcIn = "http://ics.forth.gr/isl/metrics/Dcentrality/in";
    public static final String DcOut = "http://ics.forth.gr/isl/metrics/Dcentrality/out";
    public static final String Drelevance = "http://ics.forth.gr/isl/metrics/Drelevance";
    public static final String DcOverall = "http://ics.forth.gr/isl/metrics/Dcentrality/overall";
}
