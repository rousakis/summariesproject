/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.measures;

import analysis.utils.Utils;
import static analysis.utils.Utils.fetchNeighbourNodesV2;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.diachron.detection.utils.ChangesManager;
import org.diachron.detection.utils.DatasetsManager;
import utils.multi.ChangeCounterManagerMulti;

/**
 *
 * @author rousakis
 */
public class EvolutionAnalysis {

    //update the analysis graph with the class changes 
    private static Map<String, Long> calcClassChanges(DatasetsManager dmgr, String v1, String v2) throws Exception {
        Set<String> classes = dmgr.getJDBCVirtuosoRep().getClasses(v1);
        classes.addAll(dmgr.getJDBCVirtuosoRep().getClasses(v2));
        Map<String, Long> cc = new HashMap<>();
        for (String node : classes) {
//            long classChanges = Utils.getChangesNumberPerNodeMaterialized(dmgr, v1, v2, node);
            long start = System.currentTimeMillis();
            long classChanges = ChangeCounterManagerMulti.CountChanges(dmgr.getProperties(), 4, v2, v1, node);
            System.out.println("...in " + (System.currentTimeMillis() - start) / 1000 + "sec");
            cc.put(node, classChanges);
//            break;
        }
        return cc;
    }

    //fetches the class changes considering the analysis graph
    private static Map<String, Long> fetchClassChanges(DatasetsManager dmgr, String v1, String v2) throws Exception {
//        Set<String> classes = dmgr.getJDBCVirtuosoRep().getClasses(v1);
//        classes.addAll(dmgr.getJDBCVirtuosoRep().getClasses(v2));
        Map<String, Long> cc = new HashMap<>();
        ChangesManager cmgr = new ChangesManager(dmgr, v1, v2, false);
        String graph = cmgr.getChangesOntology() + "/analysis";
        String query = "select ?class, ?raw from <" + graph + "> where { \n"
                + "?class <" + AnalysisProperties.classChanges + "> ?raw. \n"
                + "}";
        ResultSet result = dmgr.getJDBCVirtuosoRep().executeSparqlQuery(query);
        while (result.next()) {
            cc.put(result.getString(1), result.getLong(2));
        }
//        for (String node : classes) {
////            long classChanges = Utils.getChangesNumberPerNodeMaterialized(dmgr, v1, v2, node);
//            long start = System.currentTimeMillis();
//            long classChanges = ChangeCounterManagerMulti.CountChanges(dmgr.getProperties(), 4, v2, v1, node);
//            System.out.println("...in " + (System.currentTimeMillis() - start) / 1000 + "sec");
//            cc.put(node, classChanges);
////            break;
//        }
        return cc;
    }

    public static void ClassChanges(DatasetsManager dmgr, String v1, String v2) throws Exception {
        StringBuilder triplesBlock = new StringBuilder();
        long max = 0;
        Map<String, Long> cc = calcClassChanges(dmgr, v1, v2);
        for (String node : cc.keySet()) {
            long changes = cc.get(node);
            if (max < changes) {
                max = changes;
            }
        }
        for (String node : cc.keySet()) {
            triplesBlock.append("<").append(node).append("> ").
                    append("<" + AnalysisProperties.classChangesNorm + ">").
                    append(" \"").append(Utils.truncate((double) cc.get(node) / (double) max)).append("\"^^<http://www.w3.org/2001/XMLSchema#Float>.\n");
            triplesBlock.append("<").append(node).append("> ").
                    append("<" + AnalysisProperties.classChanges + ">").
                    append(" \"").append(Utils.truncate((double) cc.get(node))).append("\"^^<http://www.w3.org/2001/XMLSchema#Float>.\n");

        }
        ChangesManager cmgr = new ChangesManager(dmgr, v1, v2, false);
        dmgr.getJDBCVirtuosoRep().clearGraph(cmgr.getChangesOntology() + "/analysis", true);
        Utils.importTriplesString(triplesBlock, dmgr.getJDBCVirtuosoRep().getProperties(), cmgr.getChangesOntology() + "/analysis");
    }

    public static void NeighbourhoodChanges(DatasetsManager dmgr, String v1, String v2) throws Exception {
        Set<String> classes = dmgr.getJDBCVirtuosoRep().getClasses(v1);
        classes.addAll(dmgr.getJDBCVirtuosoRep().getClasses(v2));
        ///
        ChangesManager cmgr = new ChangesManager(dmgr, v1, v2, false);
        String chOntology = cmgr.getChangesOntology();
        Map<String, Long> cc = fetchClassChanges(dmgr, v1, v2);
        ///
        StringBuilder triplesBlock = new StringBuilder();
        Set<String> neighbours;
        Map<String, Long> nc = new HashMap<>();
        long max = 0;
        for (String node : classes) {
            neighbours = fetchNeighbourNodesV2(dmgr.getJDBCVirtuosoRep(), v1, node);
            neighbours.addAll(fetchNeighbourNodesV2(dmgr.getJDBCVirtuosoRep(), v2, node));
            long neighbChanges = 0;
            for (String neighbour : neighbours) {
                Long res = cc.get(neighbour);
                if (res != null) {
                    neighbChanges += res;
                }
            }
            if (max < neighbChanges) {
                max = neighbChanges;
            }
            nc.put(node, neighbChanges);
        }
        for (String node : nc.keySet()) {
            triplesBlock.append("<").append(node).append("> ").
                    append("<" + AnalysisProperties.neighbChangesNorm + ">").
                    append(" \"").append(Utils.truncate((double) nc.get(node) / (double) max)).append("\"^^<http://www.w3.org/2001/XMLSchema#Float>.\n");
            triplesBlock.append("<").append(node).append("> ").
                    append("<" + AnalysisProperties.neighbChanges + ">").
                    append(" \"").append(Utils.truncate((double) nc.get(node))).append("\"^^<http://www.w3.org/2001/XMLSchema#Floatk>.\n");
        }
        Utils.importTriplesString(triplesBlock, dmgr.getJDBCVirtuosoRep().getProperties(), chOntology + "/analysis");
    }

    public static void calcSummaryEvolMeasure(DatasetsManager dmgr, String v1, String v2, String sumVersProp, String sumEvolProp) throws Exception {
        ChangesManager cmgr = new ChangesManager(dmgr, v1, v2, false);
        String chOntology = cmgr.getChangesOntology();
        StringBuilder triplesBlock = new StringBuilder();

        HashMap<String, Double> v1Map = createSummaryMeasureMap(v1, sumVersProp, dmgr);
        HashMap<String, Double> v2Map = createSummaryMeasureMap(v2, sumVersProp, dmgr);

        for (String c : v1Map.keySet()) {
            Double val1 = v1Map.get(c);
            Double val2 = v2Map.get(c);
            if (val2 == null) {
                val2 = new Double(0);
            } else {
                v2Map.remove(c);
            }
            triplesBlock.append("<").append(c).append("> ").
                    append("<" + sumEvolProp + ">").
                    append(" \"").append(Math.abs(val1 - val2)).append("\"^^<http://www.w3.org/2001/XMLSchema#Float>.\n");
        }
        for (String c : v2Map.keySet()) {
            triplesBlock.append("<").append(c).append("> ").
                    append("<" + sumEvolProp + ">").
                    append(" \"").append(v2Map.get(c)).append("\"^^<http://www.w3.org/2001/XMLSchema#Float>.\n");
        }
        Utils.importTriplesString(triplesBlock, dmgr.getJDBCVirtuosoRep().getProperties(), chOntology + "/analysis");
    }

    public static HashMap<String, Double> createSummaryMeasureMap(String v1, String sumVersProp, DatasetsManager dmgr) throws SQLException {
        HashMap<String, Double> v1Map = new HashMap<>();
        String q = "select ?class, ?val from <" + v1 + "/analysis> where {\n"
                + "?class <" + sumVersProp + "> ?val. \n"
                + "}";
        ResultSet result = dmgr.getJDBCVirtuosoRep().executeSparqlQuery(q, false);
        while (result.next()) {
            String c = result.getString(1);
            double val1 = Utils.truncate(result.getDouble(2));
            v1Map.put(c, val1);
        }
        result.close();
        return v1Map;
    }

    public static void calcDCoverall(DatasetsManager dmgr, String v1, String v2) throws Exception {
        ChangesManager cmgr = new ChangesManager(dmgr, v1, v2, false);
        String chOntology = cmgr.getChangesOntology();
        String query = "sparql insert into <" + chOntology + "/analysis> {\n"
                + "?class <" + AnalysisProperties.DcOverall + "> ?dcoverall. \n"
                + "} \n" + "where { graph <" + chOntology + "/analysis> {\n"
                + "?class <" + AnalysisProperties.DcIn + "> ?dcin.\n"
                + "?class <" + AnalysisProperties.DcOut + "> ?dcout.\n"
                + "BIND ( (0.5*xsd:double(?dcin) + 0.5*xsd:double(?dcout)) as ?dcoverall).\n"
                + "}\n"
                + "}";
        dmgr.getJDBCVirtuosoRep().executeUpdateQuery(query, false);
    }

    public static Map<String, Double> fetchDChangeRankedClasses(DatasetsManager dmgr, String v1, String v2, double ccWeight, double ncWeight, int top) throws Exception {
        Map<String, Double> results = new LinkedHashMap<>();
        StringBuilder query = new StringBuilder();
        ChangesManager cmgr = new ChangesManager(dmgr, v1, v2, false);
        String chOntology = cmgr.getChangesOntology();
        query.append("select ?c, ?sum from <" + chOntology + "/analysis> where {\n")
                .append("?c <" + AnalysisProperties.classChangesNorm + "> ?cc.\n")
                .append("?c <" + AnalysisProperties.neighbChangesNorm + "> ?nc.\n")
                .append("BIND ( (" + ccWeight + " * xsd:double(?cc) + " + ncWeight + " * xsd:double(?nc)) as ?sum)\n")
                .append("} order by desc(xsd:double(?sum))");
        if (top > 0) {
            query.append(" limit " + top);
        }
        ResultSet result = dmgr.getJDBCVirtuosoRep().executeSparqlQuery(query.toString(), false);
        while (result.next()) {
            double cp = result.getDouble(2);
            results.put(result.getString(1), cp);
        }
        result.close();
        return results;
    }

    public static Map<String, Double> fetchRankdedClasses(DatasetsManager dmgr, String v1, String v2, String analProperty, int top) throws Exception {
        ChangesManager cmgr = new ChangesManager(dmgr, v1, v2, false);
        String chOntology = cmgr.getChangesOntology();
        Map<String, Double> results = new LinkedHashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("select ?class, ?value from <" + chOntology + "/analysis> where { \n").
                append("?class <" + analProperty + "> ?value.\n").
                append("} order by desc(?value)");
        if (top > 0) {
            query.append(" limit " + top);
        }
        ResultSet result = dmgr.getJDBCVirtuosoRep().executeSparqlQuery(query.toString(), false);
        while (result.next()) {
            double cp = result.getDouble(2);
            results.put(result.getString(1), cp);
        }
        result.close();
        return results;
    }

    public static void calcEvolutionMeasures(DatasetsManager dmgr, String v1, String v2) throws Exception {
//        EvolutionAnalysis.ClassChanges(dmgr, v1, v2);
        EvolutionAnalysis.NeighbourhoodChanges(dmgr, v1, v2);
//        EvolutionAnalysis.calcSummaryEvolMeasure(dmgr, v1, v2, AnalysisProperties.relevance, AnalysisProperties.Drelevance);
//        EvolutionAnalysis.calcSummaryEvolMeasure(dmgr, v1, v2, AnalysisProperties.cIn, AnalysisProperties.DcIn);
//        EvolutionAnalysis.calcSummaryEvolMeasure(dmgr, v1, v2, AnalysisProperties.cOut, AnalysisProperties.DcOut);
//        EvolutionAnalysis.calcDCoverall(dmgr, v1, v2);
    }

}
