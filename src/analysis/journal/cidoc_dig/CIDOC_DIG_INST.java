/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.journal.cidoc_dig;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.diachron.detection.utils.DatasetsManager;
import org.diachron.detection.utils.ModelType;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author rousakis
 */
public class CIDOC_DIG_INST {

    private static void InsertVersions(String folder, Properties prop, String datasetURI) throws Exception {
        DatasetsManager mgr = new DatasetsManager(prop, datasetURI);
        mgr.insertDataset(datasetURI, "cidoc dig", ModelType.ONTOLOGICAL, "input/changes_ontology/ontological/ChangesOntologySchema.n3", RDFFormat.N3);
        mgr.insertDatasetVersion(folder + "\\V1\\Cidoc-Dig_schema_INFER_instances_infer_summary.rdf", RDFFormat.RDFXML, datasetURI + "schema-infer-summary", null, datasetURI);
        mgr.insertDatasetVersion(folder + "\\V1\\Cidoc-Dig_schema_instances_summary.rdf", RDFFormat.RDFXML, datasetURI + "schema-summary", null, datasetURI);
        mgr.insertDatasetVersion(folder + "\\V2\\George.rdf", RDFFormat.RDFXML, datasetURI + "george", null, datasetURI);
        mgr.insertDatasetVersion(folder + "\\V2\\Georgis.rdf", RDFFormat.RDFXML, datasetURI + "georgis", null, datasetURI);
        mgr.insertDatasetVersion(folder + "\\V2\\Maria.rdf", RDFFormat.RDFXML, datasetURI + "maria", null, datasetURI);
        mgr.terminate();
        //////////
    }

    public static void main(String[] args) throws Exception {
        String datasetURI = "http://CIDOC_DIG/with-inst/";
        String folder = "input\\DATASET_Journal\\CIDOC_DIG\\With_Instances";
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream("config.properties");
        prop.load(inputStream);
        DatasetsManager dmgr = new DatasetsManager(prop, datasetURI);
        dmgr.deleteDataset(true, true);
        dmgr.terminate();
//        InsertVersions(folder, prop, datasetURI);
//
//        String v11 = datasetURI + "schema-infer-summary";
//        String v12 = datasetURI + "schema-summary";
//        String[] v1 = {v11, v12};
//        /////////////
//        String v21 = datasetURI + "george";
//        String v22 = datasetURI + "georgis";
//        String v23 = datasetURI + "maria";
//        String[] v2 = {v21, v22, v23};
//        /////////////
//        BufferedWriter bw = new BufferedWriter(new FileWriter(folder + "/results.txt"));
//        for (int i = 0; i < v1.length; i++) {
//            for (int j = 0; j < v2.length; j++) {
//                Utils.customCompareAndAnalyze(bw, v1[i], v2[j], prop, datasetURI);
//            }
//        }
//        bw.close();
    }
}
