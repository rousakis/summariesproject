/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

/**
 *
 * @author rousakis
 */
public class AnalysisProperties {

    public static String classPart = "http://ics.forth.gr/isl/metrics/paricipation/class";
    public static String neighbPart = "http://ics.forth.gr/isl/metrics/paricipation/neighborhood";
    public static String relevance = "http://ics.forth.gr/isl/metrics/relevance";
    public static String cIn = "http://ics.forth.gr/isl/metrics/centrality/in";
    public static String cOut = "http://ics.forth.gr/isl/metrics/centrality/out";
}
