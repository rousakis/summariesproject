/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rankings.tests;

import analysis.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import rankings.KendallImpl;
import rankings.SpearmanImpl;

/**
 *
 * @author rousakis
 */
public class CIDOC {

    public static void main(String[] args) throws IOException {
        String folder = "rankings/CIDOC/";
        String gs1 = "GS_CIDOC _Maria.txt";
        String gs2 = "GS_CIDOC_George.txt";
        String r1 = "Δchanges_0% neigh.txt";
        String r2 = "Δchanges_20% neigh.txt";
        String r3 = "Δrelevance.txt";

        ArrayList<String> rank1 = new ArrayList(Utils.readClasses(new File(folder + gs1)));
        ArrayList<String> rank2 = new ArrayList(Utils.readClasses(new File(folder + gs2)));
//        Collections.reverse(rank2);

        double kendal = KendallImpl.calculate(rank1, rank2, 0.5);
        System.out.println("Kendall Tau Distance:\t" + kendal);
        double spearman = SpearmanImpl.calculate(rank1, rank2);
        System.out.println("Spearman Distance:\t" + spearman);
    }
}
