/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rankings.tests;

import analysis.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import rankings.KendallImpl;
import rankings.PresicionImpl;
import rankings.SpearmanImpl;

/**
 *
 * @author rousakis
 */
public class Test {

    public static void main(String[] args) throws IOException {
//        CIDOC();
        HDOT();
    }

    public static void CIDOC() throws IOException {
        String folder = "rankings/CIDOC/";
        String gs1 = "GS_CIDOC_George.txt";
        String gs2 = "GS_CIDOC_Maria.txt";
        String r1 = "Δchanges_0% neigh.txt";
        String r2 = "Δchanges_25% neigh.txt";
        String r3 = "Δrelevance.txt";
        String r4 = "Δcoverall.txt";
        String r5 = "worst.txt";

//        findQualityResults(folder, gs1, r1);
//        findQualityResults(folder, gs1, r2);
//        findQualityResults(folder, gs1, r3);
//        findQualityResults(folder, gs1, r4);
//        findQualityResults(folder, gs1, r5);
        System.out.println("-\tPresicion(10)\tKendall Tau Distance (p=0)\tKendall Tau Distance (p=0.5)\tSpearman Distance");
        folder = folder + "hybrid/";
        for (String file : new File(folder).list()) {
            if (file.equals(".") || file.equals("..") || file.equals(gs1) || file.equals(gs2)) {
                continue;
            }
            findQualityResults(folder, gs2, file);
        }

    }

    public static void HDOT() throws IOException {
        String folder = "rankings/HDOT/";
        String gs = "GS_HDOT.txt";
        String r1 = "Δchanges_0% neigh.txt";
        String r2 = "Δchanges_25% neigh.txt";
        String r3 = "Δrelevance.txt";
        String r4 = "Δcoverall.txt";
        String r5 = "worst.txt";
//        findQualityResults(folder, gs, r6);

        System.out.println("-\tPresicion(10)\tKendall Tau Distance (p=0)\tKendall Tau Distance (p=0.5)\tSpearman Distance");
        folder = folder + "simple/";
        for (String file : new File(folder).list()) {
            if (file.equals(".") || file.equals("..") || file.equals(gs)) {
                continue;
            }
            findQualityResults(folder, gs, file);
        }
    }

    private static void findQualityResults(String folder, String gs, String r1) throws IOException {
        Set<String> sRank1 = Utils.readClasses(new File(folder + gs));
        Set<String> sRank2 = Utils.readClasses(new File(folder + r1));
        StringBuilder sb = new StringBuilder();
        sb.append(r1 + "\t");

//        System.out.println(r1);
        ArrayList<String> rank1 = new ArrayList(sRank1);
        ArrayList<String> rank2 = new ArrayList(sRank2);
        sb.append(PresicionImpl.calculate(sRank1, sRank2) + "\t");
        sb.append(KendallImpl.calculate(rank1, rank2, 0) + "\t");
        sb.append(KendallImpl.calculate(rank1, rank2, 0.5) + "\t");
        sb.append(SpearmanImpl.calculate(rank1, rank2));
//        System.out.println("Presicion(10)\t" + PresicionImpl.calculate(sRank1, sRank2));
//        System.out.println("Kendall Tau Distance (p=0)\t" + KendallImpl.calculate(rank1, rank2, 0));
//        System.out.println("Kendall Tau Distance (p=0.5)\t" + KendallImpl.calculate(rank1, rank2, 0.5));
//        System.out.println("Spearman Distance\t" + SpearmanImpl.calculate(rank1, rank2));
        System.out.println(sb);
    }
}
