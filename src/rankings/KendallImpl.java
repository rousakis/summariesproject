/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rankings;

import analysis.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 *
 * @author rousakis
 */
public class KendallImpl {

    public static void main(String[] args) throws IOException {
        ArrayList<String> rank1 = new ArrayList<>();
        ArrayList<String> rank2 = new ArrayList<>();
        rank1.add("A");
        rank1.add("B");
//        rank1.add("C");
//        GS.add("D");
        ////
        rank2.add("D");
        rank2.add("E");
//        rank2.add("F");
//        proposed.add("E");

//        rank1 = new ArrayList(Utils.readClasses(new File("")));
//        rank2 = new ArrayList(Utils.readClasses(new File("")));
        double totalPenalty = calculate(rank1, rank2, 0.5);

        System.out.println("Kendall distance: " + totalPenalty);

    }

    public static double calculate(ArrayList<String> rank1, ArrayList<String> rank2, double pParam) {
        ArrayList<String> all = new ArrayList<>(rank1);
        all.addAll(rank2);
        LinkedHashSet<String> clear = new LinkedHashSet<>(all);
        all = new ArrayList<>(clear);
        double totalPenalty = 0;
        for (int i = 0; i < all.size() - 1; i++) {
            for (int j = i + 1; j < all.size(); j++) {
                String ci = all.get(i);
                String cj = all.get(j);
                totalPenalty = examineCase1(rank1, ci, cj, rank2, totalPenalty);
                totalPenalty = examineCase2(rank1, ci, cj, rank2, totalPenalty);
                totalPenalty = examineCase2(rank2, ci, cj, rank1, totalPenalty);
                totalPenalty = examineCase3(rank1, ci, cj, rank2, totalPenalty);
                totalPenalty = examineCase3(rank2, ci, cj, rank1, totalPenalty);
                totalPenalty = examineCase4(rank1, ci, cj, rank2, totalPenalty, pParam);
                totalPenalty = examineCase4(rank2, ci, cj, rank1, totalPenalty, pParam);
            }
        }
        return totalPenalty;
    }

    private static double examineCase4(ArrayList<String> S1, String ci, String cj, ArrayList<String> S2, double totalPenalty, double pParam) {
        if (S1.contains(ci) && S1.contains(cj) && !S2.contains(ci) && !S2.contains(cj)) {
            totalPenalty += pParam;
//            System.out.println("case 4");
        }
        return totalPenalty;
    }

    private static double examineCase3(ArrayList<String> S1, String ci, String cj, ArrayList<String> S2, double totalPenalty) {
        if (S1.contains(ci) && !S1.contains(cj) && S2.contains(cj) && !S2.contains(ci)) {
//            System.out.println("case 3");
            totalPenalty += 1;
        }
        return totalPenalty;
    }

    private static double examineCase1(ArrayList<String> GS, String ci, String cj, ArrayList<String> proposed, double totalPenalty) {
        if (GS.contains(ci) && GS.contains(cj)
                && proposed.contains(ci) && proposed.contains(cj)) {
            if ((GS.indexOf(ci) > GS.indexOf(cj) && proposed.indexOf(ci) > proposed.indexOf(cj))
                    || (GS.indexOf(ci) < GS.indexOf(cj) && proposed.indexOf(ci) < proposed.indexOf(cj))) {
                ;
            } else {
//                System.out.println("case 1");
                totalPenalty += 1;
            }
        }
        return totalPenalty;
    }

    private static double examineCase2(ArrayList<String> S1, String ci, String cj, ArrayList<String> S2, double totalPenalty) {
        //case 2
        if (S1.contains(ci) && S1.contains(cj)) {
            if (S2.contains(ci) && !S2.contains(cj)) {
                if (S1.indexOf(ci) < S1.indexOf(cj)) {
                    ;
                } else {
//                    System.out.println("case 2");
                    totalPenalty += 1;
                }
            }
            if (S2.contains(cj) && !S2.contains(ci)) {
                if (S1.indexOf(cj) < S1.indexOf(ci)) {
                    ;
                } else {
//                    System.out.println("case 2");
                    totalPenalty += 1;
                }
            }
        }
        return totalPenalty;
    }

}
