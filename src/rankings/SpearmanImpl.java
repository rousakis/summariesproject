/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rankings;

import analysis.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 *
 * @author rousakis
 */
public class SpearmanImpl {

    public static void main(String[] args) throws IOException {
        ArrayList<String> rank1 = new ArrayList<>();
        ArrayList<String> rank2 = new ArrayList<>();

        rank1.add("A");
        rank1.add("B");
        rank1.add("C");
//        GS.add("D");
        ////
        rank2.add("D");
        rank2.add("E");
        rank2.add("F");

//        rank1 = new ArrayList(Utils.readClasses(new File("")));
//        rank2 = new ArrayList(Utils.readClasses(new File("")));

        double totalPenalty = calculate(rank1, rank2);
        System.out.println("Spearman distance: " + totalPenalty);
    }

    public static double calculate(ArrayList<String> rank1, ArrayList<String> rank2) {
        ArrayList<String> all = new ArrayList<>(rank1);
        all.addAll(rank2);
        LinkedHashSet<String> clear = new LinkedHashSet<>(all);
        all = new ArrayList<>(clear);
        double totalPenalty = 0;
        double lChoice = rank1.size();
        for (String node : all) {
            int p1 = (int) (rank1.indexOf(node) < 0 ? lChoice : rank1.indexOf(node));
            int p2 = (int) (rank2.indexOf(node) < 0 ? lChoice : rank2.indexOf(node));
            totalPenalty += Math.abs(p1 - p2);
        }
        return totalPenalty;
    }
}
