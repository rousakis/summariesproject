/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rankings;

import java.util.Set;

/**
 *
 * @author rousakis
 */
public class PresicionImpl {

    public static double calculate(Set<String> rank1, Set<String> rank2) {
        rank1.retainAll(rank2);
        return (double) rank1.size() / (double) rank2.size();
    }
}
